/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,tsx}"],
  theme: {
    container: {
      center: true,
    },
    extend: {
      colors: {
        "black-pearl": "#191D23",
        "active-item": "#E7EAEE",
        "primary-border": "#D0D5DD",
        "gradient-logo-from": "#00D995",
      },
      screens: {
        sm: { max: "640px" },
      },
    },
  },
  plugins: [],
};
