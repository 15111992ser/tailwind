import { ISelectedPeriod } from "./selectedPeriod";

export type SelectedDate =  ISelectedPeriod | Date | undefined | null