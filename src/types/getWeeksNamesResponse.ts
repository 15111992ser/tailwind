import { ICreateDateResponse } from "./createDateRespons";

export interface IGetWeeksNamesResponse {
  day: ICreateDateResponse['day'];
  dayShort: ICreateDateResponse['dayShort'];
  }