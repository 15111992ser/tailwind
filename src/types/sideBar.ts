export interface ISideBarData {
  svg: React.FunctionComponent<React.SVGProps<SVGSVGElement> & { title?: string | undefined }>;
  name: string;
  counter?: {value: number, color: "black" | "green"};
  sublist?: Array<string>;
}
