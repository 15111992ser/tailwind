import { ReactNode } from "react";

export interface ITableConfig<T> {
  key: string;
  label: string;
  style?: string;
  render: (item: T) => ReactNode | string;
}




