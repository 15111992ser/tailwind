export interface ISelectedPeriod {
  from: Date | null;
  to: Date | null;
}
