export interface IChartData {
  id: number;
  shareName: string;
  prices: IChartDataStatistics;
} 

export interface IChartDataStatistics {
  [key: string]: IChartPrice[];
}

export interface IChartPrice {
    price:  number;
    date:  Date | number;
}