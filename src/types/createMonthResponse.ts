import { ICreateDateResponse } from "./createDateRespons";

export interface ICreateMonthResponse {
    monthName: string;
    monthIndex: number;
    monthNumber: number;
    year: number;
    getDay: (dayNumber: number) => ICreateDateResponse;
    getMonthDays: () => ICreateDateResponse[];
  }