export interface ICreateDateResponse {
  date: Date;
  dayNumber: number;
  dayShort: string;
  day: string;
  dayNumberInWeek: number;
  year: number;
  yearShort: string;
  month: string;
  monthShort: string;
  monthNumber: number;
  monthIndex: number;
  timestamp: number;
  week: number;
}
