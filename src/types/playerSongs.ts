export interface ISong {
  id: number;
  title: string;
  audioUrl: any;
  progress: number;
  length: number;
  author: string;
  imgUrl: string;
  like: boolean;
}
