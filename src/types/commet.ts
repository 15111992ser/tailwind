export interface IComment{
    id: number;
    body: string;
    like: number;
    avatar: string;
    commentTime: string;
    subComments: Array<IComment>;
}



