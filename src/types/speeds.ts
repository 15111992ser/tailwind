export interface ICity {
  name: string;
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement> & { title?: string | undefined }>;
  provider: string
}

export interface ITest {
  upload: number;
  download: number;
}
