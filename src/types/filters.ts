export interface IFilter {
  name: string;
  type: "checkBox" | "radio";
  toShow: number;
  config: Array<ISelectedFilter>;
}

export interface ISelectedFilter {
  label?: string;
  value: boolean;
}
