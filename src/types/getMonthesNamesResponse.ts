import { ICreateDateResponse } from "./createDateRespons";

export interface IGetMonthesNamesResponse {
    month: ICreateDateResponse['month'];
    monthShort: ICreateDateResponse['monthShort'];
    monthIndex: ICreateDateResponse['monthIndex'];
    date: ICreateDateResponse['date'];
  }