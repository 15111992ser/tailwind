import { eachHourOfInterval, eachMinuteOfInterval, sub } from "date-fns";
import { IChartData } from "../../types/chartData";
import { randomSpeed } from "../../utils/randomSpeed";

export const fethChartData = (period: string): Array<IChartData> => {
  const dates = getDatesInPeriod(period)
  const data: IChartData[] = [
    {
      id: 1,
      shareName: "AAPL",
      prices: {
        [period]: [],
      },
    },
    {
      id: 2,
      shareName: "WMT",
      prices: {
        [period]: [],
      },
    },
    {
      id: 3,
      shareName: "MSFT",
      prices: {
        [period]: [],
      },
    },
  ];

  data.forEach(element => {
    for (const key in element.prices) {
      for (let i = 0; i < dates.length; i++) {
        element.prices[key].push({
          price: randomSpeed(3),
          date: dates[i],
        });
      }
    }
  });

  return data;
};



const getDatesInPeriod = (period: string) => {
  const d = new Date();
  switch (period) {
    case "5m":
      return eachMinuteOfInterval({
        start: getStartPeriod(period),
        end: d,
      });
    case "15m":
      return eachMinuteOfInterval({
        start: getStartPeriod(period),
        end: d,
      });
    case "4h":
      return eachMinuteOfInterval({
        start: getStartPeriod(period),
        end: d,
      }, {
        step: 10
    });
    case "1D":
      return eachHourOfInterval({
        start: getStartPeriod(period),
        end: d,
      });
    default:
      return [new Date()];
  }
};


const getStartPeriod = (period: string) => {
  const d = new Date();
  switch (period) {
    case "5m":
      return sub(d, { minutes: 5 });
    case "15m":
      return sub(d, { minutes: 15 });
    case "4h":
      return sub(d, { hours: 4 });
    case "1D":
      return sub(d, { days: 1 });
    default:
      return new Date();
  }
};

