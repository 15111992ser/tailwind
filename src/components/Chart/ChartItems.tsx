import React from "react";
import { IChartData } from "../../types/chartData";
import { checkStatistic } from "./checkStatistic";
import { ReactComponent as Arrow } from "../../assets/img/svg/chart/arrow.svg";
import { ChartLine } from "./ChartLine";

interface IChartItemProps {
  chartData: IChartData[] | undefined;
  selectedPeriod: string;
}

export const ChartItems: React.FC<IChartItemProps> = ({ chartData, selectedPeriod }) => {
  return (
    <div className="">
      {chartData?.map((el) => {
        const { name, currentPrice, isRisingPrices, percentageRiseOrFall } = checkStatistic(
          el,
          selectedPeriod
        );

        return (
          <div key={el.shareName} className="mb-10">
            <div className="flex h-12 ">
              <div className="flex  items-start  mr-10">
                <p className="mr-2">{name}</p>
                <div
                  className={` ${
                    isRisingPrices ? "bg-emerald-200 text-emerald-900" : "bg-red-500 text-red-900"
                  } flex justify-center items-center  px-2 py-1 rounded w-28 shrink-0`}
                >
                  <Arrow
                    className={` mr-2 ${
                      isRisingPrices ? "fill-emerald-900" : "fill-red-900 rotate-180"
                    }`}
                  />
                  <p className="font-semibold">{`${percentageRiseOrFall}%`}</p>
                </div>
              </div>
                <ChartLine
                  isRisingPrices={isRisingPrices}
                  data={el.prices[selectedPeriod]}
                  id={el.id}
                />
            </div>
            <p className="text-lg font-semibold">$ {currentPrice}</p>
          </div>
        );
      })}
    </div>
  );
};
