import {
  ResponsiveContainer,
  AreaChart,
  Area,
} from "recharts";

import React from "react";
import { IChartPrice } from "../../types/chartData";


interface IChartLineProps {
  id: number;
  isRisingPrices: boolean;
  data?: IChartPrice[];
}

export const ChartLine: React.FC<IChartLineProps> = ({ isRisingPrices, data, id }) => {
  const bgColor = isRisingPrices ? "#10B981" : "#FD9797";

  return (
      <ResponsiveContainer width="100%" height={"100%"}>
        <AreaChart data={data}>
          <defs>
            <linearGradient id={String(id)} x1="0" y1="0" x2="0" y2="1">
              <stop offset="0%" stopColor={bgColor} />
              <stop offset="100%" stopColor="#fff" />
            </linearGradient>
          </defs>
          <Area
            dataKey="price"
            stroke={`${isRisingPrices ? "#047857" : "#EF4444"}`}
            fill={`url(#${String(id)})`}
            type="monotone"
          />
        </AreaChart>
      </ResponsiveContainer>
  );
};