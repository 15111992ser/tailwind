import { Button } from "../Button";
import { useEffect, useState } from "react";
import { IChartData } from "../../types/chartData";
import { ChartItems } from "./ChartItems";


interface IStatisticChartProps {
  chartData: IChartData[] | undefined;
  periods: string[];
  onSelectPeriodStatistics: (period: string) => void;
}

export const ChartStatistic: React.FC<IStatisticChartProps> = ({
  periods,
  onSelectPeriodStatistics,
  chartData,
}) => {
  const [selectedPeriod, setSelectedPeriod] = useState<string>(periods[0]);

  useEffect(() => {
    onSelectPeriodStatistics(selectedPeriod);
  }, []);

  return (
    <div className=" w-full px-4 py-7 ">
      <div className="flex mb-8">
        <p className="font-semibold mr-10 w-40 shrink-0">Watchlist</p>
        <div className="flex gap-4">
          {periods.map((el) => {
            return (
              <Button
              key={el}
                onClick={() => {
                  onSelectPeriodStatistics(el);
                  setSelectedPeriod(el);
                }}
                className={`${
                  el === selectedPeriod ? "bg-slate-600" : "bg-white text-black-pearl"
                } w-12 h-7 text-white text-sm`}
              >
                {el}
              </Button>
            );
          })}
        </div>
      </div>
      <ChartItems chartData={chartData} selectedPeriod={selectedPeriod}/>
    </div>
  );
};