import { useState } from "react";
import { IChartData } from "../../types/chartData";
import { fethChartData } from "./chartData";
import { ChartStatistic } from "./ChartStatistic";


const periods = ["5m", "15m", "4h", "1D"];

export const ChartContainer = () => {
  const [chartData, setChartData] = useState<IChartData[]>();

  const onSelectPeriodStatistics = (period: string) => {
    setChartData(fethChartData(period))
  };

  return (
    <div className="w-1/2 overflow-hidden borde">
      <ChartStatistic periods={periods} onSelectPeriodStatistics={onSelectPeriodStatistics} chartData={chartData}/>
    </div>
  );
};
