import { IChartData } from "../../types/chartData";

export const checkStatistic = (statistics: IChartData, selectedPeriod: string) => {
  const arr = statistics.prices[selectedPeriod];

  const name = statistics.shareName;
  const currentPrice = arr[arr.length - 1].price
  const isRisingPrices = currentPrice > arr[0].price;
  const percentageRiseOrFall = ((currentPrice - arr[0].price) / arr[0].price * 100).toFixed(2)

  return {
    name,
    currentPrice: currentPrice.toFixed(2),
    isRisingPrices,
    percentageRiseOrFall
  }
};
