import React, { useState } from "react";
import { IComment } from "../../types/commet";
import { Button } from "../Button";
import { CommentItem } from "./CommentItem";
import { ReactComponent as Arrow } from "../../assets/img/svg/comment/arrow.svg";


interface RootCommentProps {
  addNewComment: (id: number | string, text: string) => void;
  item: IComment;
  changeLike: (id: number, status: string) => void;
}

export const RootComment: React.FC<RootCommentProps> = ({ addNewComment, item, changeLike }) => {
  const [isOpenAllMessage, setIsOpenAllMessage] = useState(false);

  return (
    <div className="w-full">
      <div className="mt-8 before:block before:absolute before:w-0.5 before:h-full before:border-l before:border-primary-border before:left-4 before:top-9 relative overflow-hidden">
        <CommentItem comment={item} addNewComment={addNewComment} changeLike={changeLike}/>
        {item.subComments?.length ? (
          <div className="ml-9">
            {item.subComments?.length  && (
              <Button
                className="text-sm font-semibold mt-5 "
                type="show"
                onClick={() => {
                  setIsOpenAllMessage(!isOpenAllMessage);
                }}
              >
                <div className="flex items-center">
                  <span className="mr-1">
                    {isOpenAllMessage
                      ? `Hide comments`
                      : `View comments`}
                  </span>
                  <Arrow
                    className={`h-4 w-4 fill-emerald-700 ${
                      isOpenAllMessage ? "rotate-180" : "rotate-135"
                    }`}
                  />
                </div>
              </Button>
            )}
            <div>
              {item.subComments?.map((el, index) => {
                return (
                  isOpenAllMessage && (
                    <div className="mt-5" key={index}>
                      {item.subComments?.length && (
                        <RootComment item={el} addNewComment={addNewComment} changeLike={changeLike}/>
                      )}
                    </div>
                  )
                );
              })}
            </div>
          </div>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};
