import React, { useState } from "react";
import { Button } from "../Button";
import { ReactComponent as Rocketship } from "../../assets/img/svg/comment/rocketship.svg";

interface ICommentsFormProps {
  addNewComment?: (message: string) => void;
  setOpenForm?: React.Dispatch<React.SetStateAction<boolean>>;
}

export const CommentsForm: React.FC<ICommentsFormProps> = ({ addNewComment, setOpenForm }) => {
  const [newMessageBody, setNewMessageBody] = useState("");

  const handleSubmit: React.FormEventHandler<HTMLFormElement> | undefined = (e) => {
    e.preventDefault();
    if (addNewComment) {
      addNewComment(newMessageBody);
      setNewMessageBody('')
      if(setOpenForm )setOpenForm(false)
    }
  };

  const handlerKeyDown: React.KeyboardEventHandler<HTMLFormElement> = (e) => {
    if (e.key === "Enter") {
      handleSubmit(e);
    }
  };

  return (
    <form className="flex items-center mt-8" onSubmit={handleSubmit} onKeyDown={handlerKeyDown}>
      <textarea
        value={newMessageBody}
        onChange={(e) => {
          setNewMessageBody(e.target.value);
        }}
        placeholder="Start typing..."
        className="p-2 text-black-pearl text-xs h-10 w-full mr-2 placeholder=Start typing... appearance-none resize-none border-2 outline-none border-primary-border rounded "
      />
      <div className=" w-10 h-10 shrink-0">
        <Button className="h-10 flex justify-center items-center">
          <Rocketship />
        </Button>
      </div>
    </form>
  );
};
