import React, { useState } from "react";
import { Comments } from "./Comments";
import { data } from "./rootCommentData";
import { IComment } from "../../types/commet";

const updateComments = (arr: Array<IComment>, id: number | string, payload: IComment | number) => {
  const copyArr = JSON.parse(JSON.stringify(arr));

  const searchComment = (array: Array<IComment>) => {
    array.forEach((elem) => {
      if (elem.id === id) {
        if (typeof payload === "object") {
          elem.subComments.push(payload);
        }
        if (typeof payload === "number") {
          elem.like = elem.like + payload
        }

      } else {
        return searchComment(elem.subComments);
      }
    });
    return array;
  };

  return searchComment(copyArr);
};

export const CommentContainer = () => {
  const [rootComment, setRootComment] = useState(data);

  const addNewComment = (id: number | string, newMessageBody: string) => {
    const newMessage = {
      avatar: "",
      body: newMessageBody,
      commentTime: "recently",
      id: Date.now(),
      like: 0,
      subComments: [],
    };
    if (id === "rootComment") {
      setRootComment(rootComment.concat(newMessage));
    } else {
      setRootComment(updateComments(rootComment, id, newMessage));
    }
  };

  const changeLike = (id: number, status: string) => {
    if(status==="inc"){
      setRootComment( updateComments(rootComment, id, 1))
    }
    if(status==="dec"){
      setRootComment( updateComments(rootComment, id, -1))
    }
  };

  return (
    <div>
      <Comments data={rootComment} addNewComment={addNewComment} changeLike={changeLike} toBack={()=>{}}/>
    </div>
  );
};