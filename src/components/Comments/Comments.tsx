import React, { useState } from "react";
import { IComment } from "../../types/commet";
import { RootComment } from "./RootComment";
import { ReactComponent as Arrow } from "../../assets/img/svg/comment/arrow.svg";
import { CommentsForm } from "./CommentsForm";
import { toShowMore } from "../../utils/toShowMore";
import { showLimit } from "./constants";
import { Button } from "../Button";

interface ICommentsProps {
  data: Array<IComment>;
  addNewComment: (id: number | string, text: string) => void;
  changeLike: (like: number, status: string) => void;
  toBack: VoidFunction
}

export const Comments: React.FC<ICommentsProps> = ({ data, addNewComment, changeLike, toBack }) => {
  const [isOpenAllMessage, setIsOpenAllMessage] = useState(false);
  return (
    <div className="p-6 bg-white">
      <div className="flex items-center" onClick={toBack}>
        <div className="w-6 h-6 border-2 border-gray-300 rounded flex justify-center items-center shrink-0 cursor-pointer">
          <Arrow className=" fill-black rotate-90" />
        </div>
        <p className="text-xl font-bold ml-3">Comments</p>
      </div>
      {data?.map((elem, index) => {
        return (
          index > data.length - 1 - toShowMore(data.length, showLimit, isOpenAllMessage) && (
            <div key={elem.id}>
              <RootComment addNewComment={addNewComment} item={elem} changeLike={changeLike} />
            </div>
          )
        );
      })}
      {data.length > showLimit && (
        <Button
          className="text-sm font-semibold mt-5 "
          type="show"
          onClick={() => {
            setIsOpenAllMessage(!isOpenAllMessage);
          }}
        >
          <div className="flex items-center">
            <span className="mr-1">
              {isOpenAllMessage
                ? `Hide all ${data.length} comments`
                : `View all ${data.length} comments`}
            </span>
            <Arrow
              className={`h-4 w-4 fill-emerald-700 ${
                isOpenAllMessage ? "rotate-180" : "rotate-135"
              }`}
            />
          </div>
        </Button>
      )}
      <CommentsForm
        addNewComment={(message) => {
          if (addNewComment) {
            addNewComment("rootComment", message);
          }
        }}
      />
    </div>
  );
};
