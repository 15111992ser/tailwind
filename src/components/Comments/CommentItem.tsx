import React, { useState } from "react";
import { IComment } from "../../types/commet";
import { Avatar } from "../Avatar";
import { ReactComponent as Like } from "../../assets/img/svg/comment/like.svg";
import { ReactComponent as Replies } from "../../assets/img/svg/comment/replies.svg";
import { CommentsForm } from "./CommentsForm";

interface ICommentProps {
  comment: IComment;
  addNewComment: (id: number | string, text: string) => void;
  changeLike: (id: number, status: string)=> void
}

export const CommentItem: React.FC<ICommentProps> = ({ comment, addNewComment, changeLike }) => {
  const [isLike, setIsLike] = useState(false);
  const [isOpenForm, setOpenForm] = useState(false);

  return (
    <div>
      <div className=" flex flex-col ">
        <div className="flex">
          <Avatar className="h-8 w-8 shrink-0" imgUrl={comment.avatar} />
          <div className="bg-gray-100 rounded-xl p-2 ml-1 w-full">
            <div className="flex justify-between items-center mb-2">
              <span className="text-slate-500 text-sm">{comment.commentTime}</span>
              <div className="cursor-pointer flex w-5 justify-between fill-transparent py-2">
                <span className="w-1 h-1 rounded-full bg-slate-300 "></span>
                <span className="w-1 h-1 rounded-full bg-slate-300"></span>
                <span className="w-1 h-1 rounded-full bg-slate-300 "></span>
              </div>
            </div>
            <p className="mb-2">{comment.body}</p>
            <div className="flex items-center ">
              <div
                className="mr-8 flex items-center cursor-pointer"
                onClick={() => {
                  changeLike(comment.id, !isLike ? "inc" : "dec")
                  setIsLike(!isLike);
                }}
              >
                <span className="text-xs mx-1 text-slate-500 font-semibold">
                  {comment.like} Like
                </span>
                <Like className={`${isLike ? "fill-blue-600" : "fill-transparent"}`} />
              </div>
              <div
                className="flex items-center cursor-pointer"
                onClick={() => {
                  setOpenForm(!isOpenForm);
                }}
              >
                <span className="text-xs mx-1 text-slate-500 font-semibold">
                  {comment.subComments.length} Replies
                </span>
                <Replies />
              </div>
            </div>
          </div>
        </div>
        <div className="ml-9">
          {isOpenForm && (
            <CommentsForm
            setOpenForm={setOpenForm}
              addNewComment={(message) => {
                addNewComment(comment.id, message);
              }}
            />
          )}
        </div>
      </div>
      <div className="pl-9"></div>
    </div>
  );
};
