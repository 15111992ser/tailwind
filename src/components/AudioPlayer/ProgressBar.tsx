import React, { useEffect, useState } from "react";
import { ISong } from "../../types/playerSongs";
import { formatSongDuration } from "../../utils/formatSongDuration";

interface IProgressBarProps {
  audioElem: React.RefObject<HTMLAudioElement>;
  currentSong: ISong;
}

export const ProgressBar: React.FC<IProgressBarProps> = ({ audioElem, currentSong }) => {
  const [progress, setProgress] = useState<number>(currentSong.progress);

  useEffect(() => {
      setProgress(currentSong.progress);
  }, [currentSong.progress, audioElem, audioElem.current?.currentTime, ]);

  return (
    <div className="w-full flex justify-between items-center">
      <div className="text-xs mr-2 shrink-0">{formatSongDuration(audioElem.current?.currentTime) || "00:00"}</div>
      <input
        type="range"
        name="range"
        className="appearance-none w-full h-1.5 cursor-pointer rounded bg-gray-200 overflow-hidden"
        min={1}
        max={100}
        value={progress}
        onChange={(e) => {
          if (audioElem.current?.currentTime) {
            audioElem.current.currentTime = (+e.target.value / 100) * currentSong.length;
          }
        }}
      />
      <div className="text-xs ml-2 shrink-0">{formatSongDuration(currentSong.length) || "00:00"}</div>
    </div>
  );
};
