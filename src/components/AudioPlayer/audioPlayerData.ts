import { ISong } from "../../types/playerSongs";
import kalina from  "../../assets/songs/kalina.mp3";
import korabil from  "../../assets/songs/korabil.mp3";
import weFromUkraine from  "../../assets/songs/weFromUkraine.mp3";

export const songsData: Array<ISong> = [
    {
        author: "Бумбокс",
        title: "Червона калина",
        audioUrl: kalina,
        progress: 0,
        length: 0,
        imgUrl: 'http://photo.oringo.com.ua/static/2022/06/02/9b4bc551050363ed8a49df7d9891148d.jpg',
        like: false,
        id: 1
    },
    {
        author: "Bakun",
        title: "руській военный корабль иди НАХУЙ",
        audioUrl: korabil,
        progress: 0,
        length: 0,
        imgUrl: 'https://images.prom.ua/3740933285_nashivka-russkij-voennyj.jpg',
        like: false,
        id: 2
        
    },


    {
        author: "ДахаБраха",
        title: "Доброго вечора - МИ З УКРАЇНИ",
        audioUrl: weFromUkraine,
        progress: 0,
        length: 0,
        imgUrl: 'https://i.pinimg.com/736x/9e/d7/ab/9ed7ab936cb30e12824f0e1e1566bdc4.jpg',
        like: false,
        id: 3
    }
]