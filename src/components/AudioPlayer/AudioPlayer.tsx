import { useEffect, useRef, useState } from "react";
import { ReactComponent as Arrow } from "../../assets/img/svg/comment/arrow.svg";
import { ReactComponent as Menu } from "../../assets/img/svg/player/menu.svg";
import { ReactComponent as Like } from "../../assets/img/svg/player/like.svg";
import { ProgressBar } from "./ProgressBar";
import { ISong } from "../../types/playerSongs";
import { Button } from "../Button";
import { AudioPlayerControl } from "./AudioPlayerControl";

interface IPlayerProps {
  songs: ISong[];
  setSongs: React.Dispatch<React.SetStateAction<ISong[]>>;
  currentSong: ISong;
  setCurrentSong: React.Dispatch<React.SetStateAction<ISong>>;
  addOrRemuveLike: () => void;
  goBack: ()=> void;
  navigationTitle: string;
}

export const AudioPlayer: React.FC<IPlayerProps> = ({
  songs,
  setSongs,
  currentSong,
  setCurrentSong,
  addOrRemuveLike,
  goBack,
  navigationTitle
}) => {
  const [isPlaying, setIsPlaying] = useState(false);
  const audioElem: React.LegacyRef<HTMLAudioElement> | undefined = useRef(null);

  useEffect(() => {
    if (isPlaying) {
      audioElem.current?.play();
    } else {
      audioElem.current?.pause();
    }
  }, [isPlaying, currentSong]);

  const onPlaying = () => {
    const duration = audioElem.current?.duration;
    const ct = audioElem.current?.currentTime;

    if (duration && ct) {
      setCurrentSong(() => ({ ...currentSong, progress: (ct / duration) * 100, length: duration }));
    }
  };

  return (
    <div className="w-full flex flex-col px-4 py-6 bg-white rounded border">
      <audio src={currentSong.audioUrl} ref={audioElem} onTimeUpdate={onPlaying} />
      <div className="flex justify-between items-center mb-3">
        <div className="flex items-center">
          <Button onClick={goBack} type="arrow" className="w-6 h-6">
            <Arrow className=" fill-black rotate-90" />
          </Button>
          <p className="text-xl font-semibold ml-3 ">{navigationTitle}</p>
        </div>
        <div className="cursor-pointer flex justify-between fill-transparent pl-2  items-center">
          <Menu />
        </div>
      </div>
      <div className="h-40 rounded mb-3 overflow-hidden">
        <img src={currentSong.imgUrl} alt="img" className="object-cover h-full w-full" />
      </div>
      <div className="mb-3">
        <div className="flex justify-between items-center ">
          <p className="mb-1 font-semibold ">{currentSong.author}</p>
          <div className="cursor-pointer">
            <Like
              className={`${currentSong.like ? "fill-red-400 stroke-red-400" : "fill-black-pearl"}`}
              onClick={addOrRemuveLike}
            />
          </div>
        </div>
        <p className="text-sm text-ellipsis overflow-hidden whitespace-nowrap">
          {currentSong.title}
        </p>
      </div>
      <div className="mb-3">
        <ProgressBar audioElem={audioElem} currentSong={currentSong} />
      </div>
      <AudioPlayerControl
        audioElem={audioElem}
        currentSong={currentSong}
        setCurrentSong={setCurrentSong}
        isPlaying={isPlaying}
        setIsPlaying={setIsPlaying}
        setSongs={setSongs}
        songs={songs}
      />
    </div>
  );
};
