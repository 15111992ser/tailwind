import { useState } from "react";
import { ISong } from "../../types/playerSongs";
import { AudioPlayer } from "./AudioPlayer";
import { songsData } from "./audioPlayerData";


export const PlayerContainer = () => {
  const [songs, setSongs] = useState<Array<ISong>>(songsData);
  const [currentSong, setCurrentSong] = useState(songs[1]);

  const addOrRemuveLike = () => {
    const copySongs = JSON.parse(JSON.stringify(songs));
    copySongs?.forEach((elem: ISong) => {
      if (elem.id === currentSong.id) {
        elem.like = elem.like ? false : true;
        setCurrentSong((prev) => ({ ...prev, like: elem.like }));
      }
    });
    setSongs(copySongs);
  };


  return (
    <div className="max-w-sm w-full m-auto">
      <AudioPlayer
        songs={songs}
        setSongs={setSongs}
        currentSong={currentSong}
        setCurrentSong={setCurrentSong}
        addOrRemuveLike={addOrRemuveLike}
        goBack={()=>{}}
        navigationTitle = "Settings"
      />
    </div>
  );
};