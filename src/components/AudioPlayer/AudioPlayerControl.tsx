import React, { useState } from "react";

import { ReactComponent as Play } from "../../assets/img/svg/player/play.svg";
import { ReactComponent as Pause } from "../../assets/img/svg/player/pause.svg";
import { ReactComponent as Mix } from "../../assets/img/svg/player/mix.svg";
import { ReactComponent as SongRepeat } from "../../assets/img/svg/player/songRepeat.svg";
import { ReactComponent as ChangeSong } from "../../assets/img/svg/player/changeSong.svg";
import { ISong } from "../../types/playerSongs";
import { changeOrderElements } from "../../utils/changeOrderElements";

interface IAudioPlayerControl{
    songs: ISong[];
    currentSong: ISong;
    setCurrentSong: React.Dispatch<React.SetStateAction<ISong>>;
    audioElem: React.RefObject<HTMLAudioElement>;
    isPlaying: boolean;
    setIsPlaying: React.Dispatch<React.SetStateAction<boolean>>;
    setSongs: React.Dispatch<React.SetStateAction<ISong[]>>;
}

export const AudioPlayerControl: React.FC<IAudioPlayerControl> = ({songs, currentSong, setCurrentSong, audioElem, setIsPlaying, isPlaying, setSongs}) => {
    const [isRepeat, setIsRepeat] = useState(false);

    const playPause = () => {
        setIsPlaying(!isPlaying);
      };

    const skipToBackOrNext = (skip: 1 | -1) => {
        const index = songs.findIndex((x) => x.title === currentSong.title);
        if (!index && skip === -1) {
          setCurrentSong(songs[songs.length - 1]);
        } else if (index === songs.length - 1 && skip === 1) {
          setCurrentSong(songs[0]);
        } else {
          setCurrentSong(songs[index + skip]);
        }
        if (audioElem.current?.currentTime) {
          audioElem.current.currentTime = 0;
        }
      };
      if (currentSong.progress === 100 && !isRepeat) {
        skipToBackOrNext(1);
      }


  return (
    <div className="flex justify-center items-center">
      <div
        className="p-1 cursor-pointer mr-3"
        onClick={() => {
          setIsRepeat(!isRepeat);
        }}
      >
        <SongRepeat className={`${isRepeat ? "fill-green-800" : "fill-black-pearl"}`} />
      </div>
      <div className="p-1 cursor-pointer">
        <ChangeSong
          onClick={() => {
            skipToBackOrNext(-1);
          }}
        />
      </div>
      <div
        onClick={playPause}
        className="p-1 cursor-pointer w-6 h-6 border border-primary-border rounded-full flex justify-center items-center mx-3"
      >
        {isPlaying ? <Pause className="w-3" /> : <Play className="w-3" onClick={playPause} />}
      </div>
      <div className="p-1 cursor-pointer">
        <ChangeSong
          className="rotate-180"
          onClick={() => {
            skipToBackOrNext(1);
          }}
        />
      </div>
      <div className="p-1 cursor-pointer ml-3">
        <Mix
          className="active:fill-green-800 "
          onClick={() => {
            setSongs([...changeOrderElements(songs)]);
          }}
        />
      </div>
    </div>
  );
};