import cn from "classnames";
import { ReactComponent as Check } from "../assets/img/svg/filter/check.svg";
import { forwardRef, useEffect, useState } from "react";

interface IInputProps  {
  type: string;
  name: string;
  placeholder?: string;
  error?: string;
  label?: string;
  inputClassName?: string;
  labelClassName?: string;
  checked?: boolean;
};

export const Input: React.FC<IInputProps> = forwardRef<HTMLInputElement, IInputProps>(
  (
    { label, error, type, inputClassName, labelClassName, checked, name, ...props },
    ref
  ) => {
    const [isChecked, setIsChacked] = useState(checked);

    useEffect(() => {
      setIsChacked(checked);
    }, [checked]);

    return (
      <div>
        {type !== "checkbox" ? (
          <>
            {label && (
              <label
                htmlFor={label}
                className={cn([
                  "mb-1 text-sm text-black-pearl font-normal cursor-pointer",
                  labelClassName,
                ])}
              >
                {label}
              </label>
            )}
            <input
              ref={ref}
              id={label}
              className={cn([
                "w-full bg-gray-50 border border-gray-300 text-black-pearl text-sm rounded focus:border-slate-500 block p-2.5 outline-none placeholder:text-slate-500 p-2",
                error && "border border-red-600",
                inputClassName,
              ])}
              type={type}
              name={name}
              {...props}
            />
            {error && <div className="text-sm text-red-600">{error}</div>}
          </>
        ) : (
            <label className="flex items-center cursor-pointer ">
              <div className="relative flex items-center">
                <input
                  className={cn([
                    "h-4 w-4 shrink-0 appearance-none border-2 border-primary-border rounded-sm",
                    isChecked && " bg-emerald-700 border-emerald-700",
                  ])}
                  type="checkbox"
                  name={name}
                  ref={ref}
                  {...props}
                />
                <Check className="w-3 h-2 absolute left-1/2 top-1/2 -translate-y-1/2 -translate-x-1/2" />
              </div>
              <div className="ml-3.5 text-ellipsis overflow-hidden whitespace-nowrap">{label}</div>
            </label>
        )}
      </div>
    );
  }
);
