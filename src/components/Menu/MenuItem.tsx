import React, { useLayoutEffect, useRef } from "react";

interface IMenuItemProps {
  onLayout: (width: number, marginLeft: number) => void;
  tabName: string;
  setSelectedTab: () => void;
  isActive: boolean;
  SvgComponent?: React.FunctionComponent<
    React.SVGProps<SVGSVGElement> & { title?: string | undefined }
  >;
  styleTabItem?: string
}

export const MenuItem: React.FC<IMenuItemProps> = ({
  tabName,
  setSelectedTab,
  onLayout,
  isActive,
  SvgComponent,
  styleTabItem = ""
}) => {
  const tabRef = useRef<HTMLHeadingElement>(null);

  useLayoutEffect(() => {
    if (tabRef.current) {
      const marginLeft = tabRef.current?.getBoundingClientRect().left;
      onLayout(tabRef.current?.offsetWidth, marginLeft);
    }
  }, []);

  let baseClassesTabItem = [
    isActive && "font-semibold",
  ];

  if (styleTabItem) {
    baseClassesTabItem = [...baseClassesTabItem, ...styleTabItem.split(" ")];
  }

  return (
    <div ref={tabRef} className="cursor-pointer px-2 " onClick={setSelectedTab}>
      {SvgComponent && (
        <SvgComponent
          className={`h-6 w-6 block mx-auto mb-1 ${
            isActive ? "fill-bg-emerald-700 stroke-bg-emerald-700" : "fill-white stroke-black-pearl"
          }`}
          id={tabName}
        />
      )}
      <div className={baseClassesTabItem.join(" ")}>{tabName}</div>
    </div>
  );
};
