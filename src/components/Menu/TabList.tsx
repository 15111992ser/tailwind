import { ReactComponent as Home } from "../../assets/img/svg/menu/home.svg";
import { ReactComponent as Headlines } from "../../assets/img/svg/menu/headlines.svg";
import { ReactComponent as Bookmarks } from "../../assets/img/svg/menu/bookmarks.svg";
import { ReactComponent as Explore } from "../../assets/img/svg/menu/explore.svg";
import { ReactComponent as Search } from "../../assets/img/svg/menu/search.svg";

export const list = [
  {
    name: "Home",
    svg: Home,
  },
  {
    name: "Headlines",
    svg: Headlines,
  },
  {
    name: "Bookmarks",
    svg: Bookmarks,
  },
  {
    name: "Explore",
    svg: Explore,
  },
  {
    name: "Search",
    svg: Search,
  },
];
