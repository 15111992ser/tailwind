import { useLayoutEffect, useRef, useState } from "react";
import { MenuItem } from "./MenuItem";

interface ITab {
  width: number;
  index: number;
  name: string;
  marginLeft: number;
}

interface ITabs {
  [key: number]: ITab;
}

interface IMenuProps {
  tabList: Array<{ name: string; svg?: React.FunctionComponent<React.SVGProps<SVGSVGElement> & {title?: string | undefined;}> }>;
  onTabPress?: (id: number) => void;
  styleBottomLine?: string;
  styleTabItem?: string;
}

export const Menu: React.FC<IMenuProps> = ({ tabList, onTabPress = () => {}, styleBottomLine = "",  styleTabItem = ""}) => {
  const tabsContainerRef = useRef<HTMLHeadingElement | null>(null);
  const [tabs, setTabs] = useState<ITabs>({});
  const [selectedTabId, setSelectedTabId] = useState(0);
  const [marginLeftTabsContainer, setMarginLeftTabsContainer] = useState<number | undefined>();
  const selectedTab = tabs[selectedTabId];

  const onLayout = ({ width, index, name, marginLeft }: ITab) => {
    setTabs((prev) => ({
      ...prev,
      [index]: { index, name, width, marginLeft },
    }));
  };

  useLayoutEffect(() => {
    setMarginLeftTabsContainer(tabsContainerRef.current?.getBoundingClientRect().left);
  }, []);

  let baseClassesBottomLine = [
    "h-1 rounded-lg duration-300 mt-5 relative"
  ];

  if (styleBottomLine) {
    baseClassesBottomLine = [ ...baseClassesBottomLine, ...styleBottomLine.split(" ")];
  }

  return (
    <div className=" p-3 text-black-pearl">
      <div ref={tabsContainerRef} className="flex justify-between relative">
        {tabList.map((elem, index) => (
          <MenuItem
            onLayout={(width, marginLeft) => {
              onLayout({ width, index, name: elem.name, marginLeft });
            }}
            setSelectedTab={() => {
              setSelectedTabId(index);
              onTabPress(index);
            }}
            tabName={elem.name}
            isActive={index === selectedTabId}
            SvgComponent={elem.svg}
          />
        ))}
      </div>
      <div
        className={baseClassesBottomLine.join(" ")}
        style={{
          width: selectedTab?.width * 1.5,
          transform: `translate(${
            selectedTab?.marginLeft - (marginLeftTabsContainer ?? 0) - selectedTab?.width / 4
          }px)`,
        }}
      ></div>
    </div>
  );
};
