import React from 'react'
import { Button } from '../Button';
import { ReactComponent as Arrow } from "../../assets/img/svg/comment/arrow.svg";

interface ICalendarHeaderProps{
    onClickAgoBlock: (totalMonth: number)=> void
    onClickArrow: (direction: "right" | "left") => void
    state: any;
}

export const CalendarHeader: React.FC<ICalendarHeaderProps> = ({onClickArrow, onClickAgoBlock, state}) => {
  return (
    <div>
        <div className="flex justify-between mb-4">
        <Button
          onClick={() => {
            onClickArrow("left");
          }}
          type="arrow"
          className="w-6 h-6"
        >
          <Arrow className=" fill-slate-500 rotate-90" />
        </Button>
        <div className="font-semibold">
          {state.monthesNames[state.selectedMonth.monthIndex].month} {state.selectedYear}
        </div>
        <Button
          onClick={() => {
            onClickArrow("right");
          }}
          type="arrow"
          className="w-6 h-6"
        >
          <Arrow className=" fill-slate-500 -rotate-90" />
        </Button>
      </div>
      <div className="flex justify-between items-center text-xs mb-3">
        <button
          onClick={() => {
            onClickAgoBlock(3);
          }}
          className="block bg-gray-200 rounded w-20 h-7 cursor-pointer"
        >
          3 month ago
        </button>
        <button
          onClick={() => {
            onClickAgoBlock(6);
          }}
          className="block bg-gray-200 rounded w-20 h-7 mx-1.5 cursor-pointer"
        >
          6 months ago
        </button>
        <button
          onClick={() => {
            onClickAgoBlock(12);
          }}
          className="block bg-gray-200 rounded w-20 h-7 cursor-pointer"
        >
          1 Year Ago
        </button>
      </div>
    </div>
  )
}