import { SelectedDate } from "../../types/selectedDate";
import { Calendar } from "./Calendar";

export const CalendarContainer = () => {
  const onSetDate = (date: SelectedDate | Date) => {
    alert(JSON.stringify(date));
  };

  const onCancel = () => {
    alert("onCancel");
  };

  return (
    <div className="w-80">
      <Calendar
        isRange
        selectedDate={{from: new Date(2022, 7, 15), to: new Date(2022, 7, 20)}}
        onSetDate={onSetDate}
        onCancel={onCancel}
        firstWeekDayNumber={1}
        locale="en-US"
      />
    </div>
  );
};