import { SelectedDate } from "../../types/selectedDate";
import { ISelectedPeriod } from "../../types/selectedPeriod";

export const getCorrectPeriod = (period:SelectedDate, isRange?: boolean  ) => {
  if(isRange ){
    return period as ISelectedPeriod;
  }
  if (!isRange && period instanceof Date) {
    return { from: period, to: null } as ISelectedPeriod;
  }
};
