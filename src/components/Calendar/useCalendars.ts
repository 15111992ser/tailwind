import React, { useMemo } from "react";
import { ICreateDateResponse } from "../../types/createDateRespons";
import { ICreateMonthResponse } from "../../types/createMonthResponse";

import { Direction } from "../../types/direction";
import { IGetMonthesNamesResponse } from "../../types/getMonthesNamesResponse";
import { createDate } from "../../utils/calendar/createDate";
import { createMonth } from "../../utils/calendar/createMonth";
import { getMonthesNames } from "../../utils/calendar/getMonthNames";
import { getMonthNumberOfDays } from "../../utils/calendar/getMonthNumberOfDays";
import { getWeekDaysNames } from "../../utils/calendar/getWeekDaysNames";

interface UseCalendarParams {
  locale?: string;
  firstWeekDayNumber?: number;
}

interface IUseCalendar {
  state: {
    calendarDays: ICreateDateResponse[];
    weekDaysNames: { day: string; dayShort: string }[];
    monthesNames: IGetMonthesNamesResponse[];
    selectedMonth: ICreateMonthResponse;
    selectedYear: number;
  };
  functions: {
    onClickArrow: (direction: Direction) => void;
    onClickAgoBlock: (timeAgo: number) => void;
  };
}

const DAYS_IN_WEEK = 7;

export const useCalendar = ({
  locale = "default",

  firstWeekDayNumber = 2,
}: UseCalendarParams): IUseCalendar => {
  const [currentDate, setCurrentDate] = React.useState(createDate({date: new Date()}));
  const [selectedMonth, setSelectedMonth] = React.useState(
    createMonth({ date: new Date(currentDate.year, currentDate.monthIndex), locale })
  );

  const [selectedYear, setSelectedYear] = React.useState(currentDate.year);

  const monthesNames = useMemo(() => getMonthesNames(locale), [locale]);
  const weekDaysNames = useMemo(() => getWeekDaysNames(firstWeekDayNumber, locale), [locale]);
  const days = useMemo(() => selectedMonth.getMonthDays(), [selectedMonth, selectedYear]);
  const calendarDays = useMemo(() => {
    const monthNumberOfDays = getMonthNumberOfDays(selectedMonth.monthIndex, selectedYear);

    const prevMonthDays = createMonth({
      date: new Date(selectedYear, selectedMonth.monthIndex - 1),
      locale,
    }).getMonthDays();

    const nextMonthDays = createMonth({
      date: new Date(selectedYear, selectedMonth.monthIndex + 1),
      locale,
    }).getMonthDays();

    const firstDay = days[0];
    const lastDay = days[monthNumberOfDays - 1];

    const shiftIndex = firstWeekDayNumber - 1;
    const numberOfPrevDays =
      firstDay.dayNumberInWeek - 1 - shiftIndex < 0
        ? DAYS_IN_WEEK - (firstWeekDayNumber - firstDay.dayNumberInWeek)
        : firstDay.dayNumberInWeek - 1 - shiftIndex;

    const numberOfNextDays =
      DAYS_IN_WEEK - lastDay.dayNumberInWeek + shiftIndex > 6
        ? DAYS_IN_WEEK - lastDay.dayNumberInWeek - (DAYS_IN_WEEK - shiftIndex)
        : DAYS_IN_WEEK - lastDay.dayNumberInWeek + shiftIndex;

    const totalCalendarDays = days.length + numberOfPrevDays + numberOfNextDays;

    const result = [];

    for (let i = 0; i < numberOfPrevDays; i += 1) {
      const inverted = numberOfPrevDays - i;
      result[i] = prevMonthDays[prevMonthDays.length - inverted];
    }

    for (let i = numberOfPrevDays; i < totalCalendarDays - numberOfNextDays; i += 1) {
      result[i] = days[i - numberOfPrevDays];
    }

    for (let i = totalCalendarDays - numberOfNextDays; i < totalCalendarDays; i += 1) {
      result[i] = nextMonthDays[i - totalCalendarDays + numberOfNextDays];
    }

    return result;
  }, [selectedMonth.year, selectedMonth.monthIndex, selectedYear]);

  const onClickArrow = (direction: Direction) => {
    const monthIndex =
      direction === "left" ? selectedMonth.monthIndex - 1 : selectedMonth.monthIndex + 1;
    if (monthIndex === -1) {
      const year = selectedYear - 1;
      setSelectedYear(year);

      return setSelectedMonth(createMonth({ date: new Date(selectedYear - 1, 11), locale }));
    }

    if (monthIndex === 12) {
      const year = selectedYear + 1;
      setSelectedYear(year);

      return setSelectedMonth(createMonth({ date: new Date(year, 0), locale }));
    }

    setSelectedMonth(createMonth({ date: new Date(selectedYear, monthIndex), locale }));
  };

  const onClickAgoBlock = (timeAgo: number) => {
    const monthIndex = selectedMonth.monthIndex - timeAgo;

    if (monthIndex < 0) {
      const year = selectedYear - 1;
      setSelectedYear(year);

      return setSelectedMonth(
        createMonth({ date: new Date(selectedYear - 1, 12 + monthIndex), locale })
      );
    }

    setSelectedMonth(createMonth({ date: new Date(selectedYear, monthIndex), locale }));
  };

  return {
    state: {
      calendarDays,
      weekDaysNames,
      monthesNames,
      selectedMonth,
      selectedYear,
    },
    functions: {
      onClickArrow,
      onClickAgoBlock,
    },
  };
};