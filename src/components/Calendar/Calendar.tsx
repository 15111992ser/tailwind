import React, { useEffect, useState } from "react";
import { ReactComponent as Ok } from "../../assets/img/svg/calendar/ok.svg";
import { checkIsToday } from "../../utils/calendar/checkIsToday";
import { checkIsPeriod } from "../../utils/calendar/checkIsPeriod";
import { Button } from "../Button";
import { useCalendar } from "./useCalendars";
import { CalendarHeader } from "./CalendarHeader";
import { getCorrectPeriod } from "./getCorrectPeriod";
import { SelectedDate } from "../../types/selectedDate";
import { ISelectedPeriod } from "../../types/selectedPeriod";

interface ICalendarProps {
  selectedDate?: SelectedDate;
  isRange?: boolean;
  onSetDate: (data: SelectedDate) => void;
  onCancel: () => void;
  locale?: string;
  firstWeekDayNumber?: number;
}

export const Calendar: React.FC<ICalendarProps> = ({
  selectedDate,
  isRange,
  onSetDate,
  onCancel,
  locale = "default",
  firstWeekDayNumber = 2,
}) => {
  const { functions, state } = useCalendar({
    locale,
    firstWeekDayNumber,
  });

  const [period, setPeriod] = useState(getCorrectPeriod(selectedDate, isRange));
  const [isDisableSetBtn, setIsDisableSetBtn] = useState(true);

  useEffect(() => {
    setPeriod(getCorrectPeriod(selectedDate, isRange));
  }, [selectedDate]);

  const selectPeriod = (date: Date) => {
    if (isRange) {
      if ((period?.from && period.to) || (!period?.from && !period?.to)) {
        setPeriod({ from: date, to: null });
      }
      if (!period?.from && period?.to) {
        setPeriod({ ...period, from: date });
      }
      if (period?.from && !period?.to) {
        setPeriod({ ...period, to: date });
      }
    } else {
      setPeriod(() => ({ from: date, to: null }));
    }
  };

  const handleSetDate = () => {
    if ((!period?.from || !period?.to) && isRange) {
      alert("Выбери вторую дату");
    }  if (period?.from && period?.to) {
      onSetDate(period);
    }
    if(!isRange){
      onSetDate(period?.from || period?.to);
    }
  };

  return (
    <div className="p-4  flex flex-col">
      <CalendarHeader
        onClickAgoBlock={functions.onClickAgoBlock}
        onClickArrow={functions.onClickArrow}
        state={state}
      />
      <div>
        <div className="mb-4">
          <div className="py-2.5 border-t-2 border-primary-border">
            <div className="grid gap-px grid-cols-7">
              {state.weekDaysNames.map((weekDaysName) => (
                <div className="grid justify-center items-center" key={weekDaysName.dayShort}>
                  {weekDaysName.dayShort}
                </div>
              ))}
            </div>
          </div>
          <div className="grid gap-px grid-cols-7 h-44">
            {state.calendarDays.map((day) => {
              const isToday = checkIsToday(day.date);
              const isAdditionalDay = day.monthIndex !== state.selectedMonth.monthIndex;
              const { isInPeriod, startOrEndPeriod } = checkIsPeriod(day.date, period);

              return (
                <div
                  key={`${day.dayNumber}-${day.monthIndex}`}
                  aria-hidden
                  onClick={() => {
                    selectPeriod(day.date);
                    setIsDisableSetBtn(false);
                  }}
                  className={`${isAdditionalDay && "text-gray-300"} ${
                    isToday && "border-2 border-primary-border"
                  } ${isInPeriod && "bg-emerald-300 text-white"} ${
                    startOrEndPeriod && "bg-emerald-700 text-white"
                  } grid justify-center items-center rounded cursor-pointer hover:bg-emerald-700`}
                >
                  {day.dayNumber}
                </div>
              );
            })}
          </div>
        </div>
        <div className="flex">
          <Button
            onClick={() => {
              setPeriod({ from: null, to: null });
              onCancel();
              setIsDisableSetBtn(true);
            }}
            className="mr-4 h-11 bg-gray-200 text-slate-600"
          >
            Cancel
          </Button>
          <Button
            disabled={isDisableSetBtn}
            onClick={() => {
              handleSetDate();
              setIsDisableSetBtn(true);
            }}
            className="h-11 flex justify-center items-center"
          >
            <Ok className="mr-1.5" />
            Set date
          </Button>
        </div>
      </div>
    </div>
  );
};
