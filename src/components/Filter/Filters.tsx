import React, { useEffect, useState } from "react";
import { IFilter } from "../../types/filters";
import { RadioGrup } from "./RadioGroup";
import { CheckBoxGrup } from "./CheckBoxGroup";

interface IFiltersProps {
  filtersList: Array<IFilter>;
  handlerFilters: any;
}

export const Filters: React.FC<IFiltersProps> = ({ filtersList, handlerFilters }) => {
  const [currentSelectedFilter, setCurrentSelectedFilter] = useState({});

  useEffect(() => {
    handlerFilters(currentSelectedFilter);
  }, [currentSelectedFilter]);

  return (
    <div className="bg-white p-6 shadow-xl h-screen">
      <h4 className="text-2xl font-bold mb-8">Filters</h4>
      <ul>
        {filtersList.map((elem) => {
          return (
            <div key={elem.name}>
              {elem.type === "radio" && (
                <RadioGrup
                  config={elem.config}
                  toShow={elem.toShow}
                  name={elem.name}
                  setCurrentSelectedFilter={setCurrentSelectedFilter}
                />
              )}
              {elem.type === "checkBox" && (
                <CheckBoxGrup
                  config={elem.config}
                  toShow={elem.toShow}
                  name={elem.name}
                  setCurrentSelectedFilter={setCurrentSelectedFilter}
                />
              )}
            </div>
          );
        })}
      </ul>
    </div>
  );
};
