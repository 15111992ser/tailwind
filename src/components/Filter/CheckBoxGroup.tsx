import React, { useEffect, useState } from "react";
import { ISelectedFilter } from "../../types/filters";
import { CheckBox } from "../CheckBox";
import { ReactComponent as Arrow } from "../../assets/img/svg/filter/arrow.svg";
import { Button } from "../Button";
import { toShowMore } from "../../utils/toShowMore";

interface IFilterItemProps {
  config: Array<ISelectedFilter>;
  toShow: number;
  name: string;
  setCurrentSelectedFilter: React.Dispatch<React.SetStateAction<{}>>;
}

export const CheckBoxGrup: React.FC<IFilterItemProps> = ({
  config,
  toShow,
  name,
  setCurrentSelectedFilter,
}) => {
  const [isOpenFilter, setIsOpenFilter] = useState(false);
  const [isOpenShowMore, setIsOpenShowMore] = useState(false);
  const [selectedCheckBox, setSelectedCheckBox] = useState<string[]>([]);

  useEffect(()=>{
    setCurrentSelectedFilter(prev=> ({...prev, [name]: selectedCheckBox}))
  }, [selectedCheckBox])

  const onClickCheckBox = (isChecked: boolean, value: string) =>{
    if (!selectedCheckBox?.includes(value)) {
      if (setSelectedCheckBox) {
        setSelectedCheckBox((prev) => [...prev, value]);
      }
    } else {
      const filter = selectedCheckBox?.filter((el) => el !== value);
      if (setSelectedCheckBox) {
        setSelectedCheckBox(filter);
      }
    }
  }

  return (
    <>
      <div className="mb-4 font-bold text-lg flex justify-between items-center">
        <span>{name}</span>
        <div
          className="p-1 cursor-pointer"
          onClick={() => {
            setIsOpenFilter(!isOpenFilter);
          }}
        >
          <Arrow className={`fill-slate-500 ${isOpenFilter ? "rotate-180" : "rotate-135"}`} />
        </div>
      </div>
      {isOpenFilter && (
        <>
          {config.map((elem, index) => (
            <div key={elem.label}>
              {index <= toShowMore(config.length, toShow, isOpenShowMore) && (
                <div className="mb-4">
                  {
                    <CheckBox
                      config={elem}
                      onClickCheckBox={onClickCheckBox}
                    />
                  }
                </div>
              )}
            </div>
          ))}
          <Button
            className="h-5 mb-6 font-semibold"
            type={"show"}
            onClick={() => {
              setIsOpenShowMore(!isOpenShowMore);
            }}
          >
            <div className="flex items-center">
              <span className="mr-1">{isOpenShowMore ? "Hide" : "Show more"}</span>
              <Arrow
                className={`h-4 w-4 fill-emerald-700 ${
                  isOpenShowMore ? "rotate-180" : "rotate-135"
                }`}
              />
            </div>
          </Button>
        </>
      )}
    </>
  );
};