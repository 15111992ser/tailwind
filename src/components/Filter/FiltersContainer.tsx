import React, { useState } from "react";
import { IFilter } from "../../types/filters";
import { Filters } from "./Filters";
import { filtersList } from "./filtersList";

export const FiltersContainer = () => {
  const [filters, setFilter] = useState(filtersList);


  const handlerFilters = (currentSelectedFilter: any) => {
    const copyfilters =  JSON.parse(JSON.stringify(filters))

    copyfilters.forEach((elem: IFilter) => {
      elem.config.forEach((filter) => {
        if (currentSelectedFilter[elem.name]?.includes(filter.label)) {
          filter.value = true;
        } else {
          filter.value = false;
        }
      });
    });
    setFilter(copyfilters);
  };

  return (
    <div>
      <Filters filtersList={filters} handlerFilters={handlerFilters} />
    </div>
  );
};
