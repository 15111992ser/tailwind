import React, { useEffect, useState } from "react";
import { ISelectedFilter } from "../../types/filters";
import { Button } from "../Button";
import { Radio } from "../Radio";
import { ReactComponent as Arrow } from "../../assets/img/svg/filter/arrow.svg";
import { toShowMore } from "../../utils/toShowMore";

interface ICheckBoxProps {
  config: Array<ISelectedFilter>;
  toShow: number;
  name: string;
  setCurrentSelectedFilter: React.Dispatch<React.SetStateAction<{}>>
}

export const RadioGrup: React.FC<ICheckBoxProps> = ({ config, toShow, name, setCurrentSelectedFilter }) => {
  const [isOpenFilter, setIsOpenFilter] = useState(false);
  const [isOpenShowMore, setIsOpenShowMore] = useState(false);
  const [selectedRadio, setSelectedBtn] = useState<string>();

  useEffect(()=>{
    setCurrentSelectedFilter(prev=> ({...prev, [name]: selectedRadio}))
  }, [selectedRadio])

  useEffect(() => {
    let str;
    config.forEach((el) => {
      if (el.value) {
        str = el.label;
      }
    });
    setSelectedBtn(str);
  }, []);

  return (
    <>
      <div className="mb-4 font-bold text-lg flex justify-between items-center">
        <span>{name}</span>
        <div
          className="p-1 cursor-pointer"
          onClick={() => {
            setIsOpenFilter(!isOpenFilter);
          }}
        >
          <Arrow className={`fill-slate-500 ${isOpenFilter ? "rotate-180" : "rotate-135"}`} />
        </div>
      </div>
      {isOpenFilter && <>
        {config.map((el, index) => {
          return (
            <div key={el.label}>
              {index <= toShowMore(config.length, toShow, isOpenShowMore) && (
                <div className="mb-4">
                  <Radio
                    name={el.label}
                    setSelectedBtn={setSelectedBtn}
                    checked={selectedRadio === el.label}
                  />
                </div>
              )}
            </div>
          );
        })}
        <Button
          className="h-5 mb-6 font-semibold"
          type={"show"}
          onClick={() => {
            setIsOpenShowMore(!isOpenShowMore);
          }}
        >
          <div className="flex items-center">
            <span className="mr-1">{isOpenShowMore ? "Hide" : "Show more"}</span>
            <Arrow
              className={`h-4 w-4 fill-emerald-700 ${isOpenShowMore ? "rotate-180" : "rotate-135"}`}
            />
          </div>
        </Button>
      </>}
    </>
  );
};