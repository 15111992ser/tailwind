import { IFilter } from "../../types/filters";

export const filtersList: Array<IFilter> = [
  {
    name: "Deals",
    type: "checkBox",
    toShow: 1,
    config: [
      { label: "Best value from customers", value: false },
      { label: "Members only deals", value: false },
      { label: "Something", value: false },
    ],
  },
  {
    name: "Your Budget",
    type: "checkBox",
    toShow: 3,
    config: [
      { label: "Less than $25", value: false },
      { label: "$150 - $250", value: false },
      { label: "$250 - $350", value: false },
      { label: "Greater than $500", value: false },
      { label: "Something", value: false },
      { label: "Something text", value: false },
    ],
  },
  {
    name: "Rating",
    type: "radio",
    toShow: 4,
    config: [
      { label: "1 Star", value: false },
      { label: "2 Stars", value: false },
      { label: "3 Stars", value: false },
      { label: "4 Stars", value: false },
      { label: "5 Stars", value: false },
      { label: "6 Stars", value: false },
      { label: "7 Stars", value: false },
    ],
  },
];
