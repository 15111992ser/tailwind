import React from 'react'

interface IAvatarProps {
    imgUrl?: string;
    className?: string;
}

export const Avatar:React.FC<IAvatarProps> = ({imgUrl, className}) => {
    let baseClasses = [
        "h-10",
        "w-10",
        "rounded-full overflow-hidden",
      ];
    
      if (className) {
        baseClasses = [...baseClasses, ...className.split(" ")];
      }

  return (
    <div className={baseClasses.join(" ")}>
         <img src={imgUrl ? imgUrl : "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBtSBRqnZmVOLoutH3YlzXuwx8TdozXWEMceslegDyzO7k8qU6z_hNHSP85MD0geUYELY&usqp=CAU"} alt="ava" className='w-full h-full' />
    </div>
  )
}
