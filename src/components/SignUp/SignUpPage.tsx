import { Stepper } from "./Stepper";
import { Regisrtration } from "./Regisrtration";
import { useState } from "react";
import { BuildTeam } from "./BuildTeam";
import { Profile } from "./Profile";
import { changeSteppDirection } from "../../types/changeSteppDirection";

export const SignUpPage = () => {
  const [currentStep, setCurrentStep] = useState(1);

  const steps = ["Sign up", "Build Team", "Profile"];

  const handleChangeStep = (direction: changeSteppDirection) => {
    let newStep = currentStep;

    direction === "next" ? newStep++ : newStep--;
    newStep > 0 && newStep <= steps.length && setCurrentStep(newStep);
  };

  const displayStep = (step: number) => {
    switch (step) {
      case 1:
        return <Regisrtration handleChangeStep={handleChangeStep} />;
      case 2:
        return <BuildTeam handleChangeStep={handleChangeStep}/>;
      case 3:
        return <Profile handleChangeStep={handleChangeStep}/>;

      default:
    }
  };

  return (
    <div className="w-1/4 border py-8 px-6 mx-auto">
      <div className="mb-8">
        <Stepper steps={steps}  currentStep={currentStep}/>
      </div>
      {displayStep(currentStep)}
    </div>
  );
};