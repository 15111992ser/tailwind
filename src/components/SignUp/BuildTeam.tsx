import React from 'react'
import { changeSteppDirection } from '../../types/changeSteppDirection'

interface IBuildTeamProps{
  handleChangeStep: (description: changeSteppDirection)=> void
}

export const BuildTeam: React.FC<IBuildTeamProps> = ({handleChangeStep}) => {
  return (
    <div>
      <button onClick={()=>{handleChangeStep("back")}} className='p-3'>Back</button>
      <button onClick={()=>{handleChangeStep("next")}} className='p-3'>Next</button>
    </div>
  )
}
