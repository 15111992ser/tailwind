import React from 'react'
import { changeSteppDirection } from '../../types/changeSteppDirection'

interface IProfileProps{
  handleChangeStep: (description: changeSteppDirection)=> void
}

export const Profile: React.FC<IProfileProps> = ({handleChangeStep}) => {
  return (
    <div>
      <button onClick={()=>{handleChangeStep("back")}} className='p-3'>back</button>
    </div>
  )
}
