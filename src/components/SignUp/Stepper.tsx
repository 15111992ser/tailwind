import React, { useState, useEffect, useRef } from "react";

interface IStepperProps {
  steps: string[];
  currentStep: number;
}

interface IStep {
  description: string;
  selected: boolean;
}

export const Stepper: React.FC<IStepperProps> = ({ steps, currentStep }) => {
  const [newStep, setNewStep] = useState<IStep[]>();
  const stepsRef = useRef<IStep[]>([]);

  const updateStep = (stepNumber: number, steps: IStep[]) => {
    const newSteps = [...steps];

    let count = 0;
    while (count < newSteps.length) {
      if (count <= stepNumber) {
        newSteps[count] = {
          ...newSteps[count],
          selected: true,
        };
        count++;
      } else {
        newSteps[count] = {
          ...newSteps[count],
          selected: false,
        };
        count++;
      }
    }

    return newSteps;
  };

  useEffect(() => {
    const stepsState = steps.map((step, index) =>
      Object.assign(
        {},
        {
          description: step,
          completed: false,
          selected: index === 0 ? true : false,
        }
      )
    );

    stepsRef.current = stepsState;
    const current = updateStep(currentStep - 1, stepsRef.current);

    setNewStep(current);
  }, [steps, currentStep]);

  const stepsDisplay = newStep?.map((step, index) => {
    return (
      <>
        <div  className="flex items-center">
          <div className="relative flex items-center text-black">
            <div
              className={`flex-shrink-0 rounded border border-emerald-600 border-opacity-50 h-6 w-6 flex items-center justify-center py-2  mr-1  ${
                step.selected ? "bg-emerald-700 " : ""
              }`}
            >
              <span
                className={`  font-semibold text-sm  ${
                  step.selected
                    ? "text-white font-semibold text-opacity-100"
                    : "text-emerald-600 text-opacity-50"
                }`}
              >
                {index + 1}
              </span>
            </div>
            <div
              className={`text-center text-xs font-medium uppercase ${
                step.selected ? "text-gray-900" : "text-gray-400"
              }`}
            >
              {step.description}
            </div>
          </div>
        </div>
        {index !== newStep.length - 1 ? (
          <div className="flex-1 border-dashed border-t border-black-pearl mx-2"></div>
        ) : (
          ""
        )}
      </>
    );
  });

  return <div className="flex justify-between items-center">{stepsDisplay}</div>;
};
