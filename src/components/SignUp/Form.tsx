import { Controller, useForm } from "react-hook-form";
import { Button } from "../Button";
import { Input } from "../Input";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { changeSteppDirection } from "../../types/changeSteppDirection";
import ReactSelect from "react-select";

interface IOption {
  value: string;
  label: string;
}

const options: IOption[] = [
  {
    value: "friends & family",
    label: "Friends & Family",
  },
  {
    value: "text",
    label: "text",
  },
];

export const getValue = (value: string) =>
  value ? options.find((option) => option.value === value) : "";

const schema = yup.object().shape({
  email: yup.string().email().required("Email is required"),
  firstName: yup.string().required("Name is required"),
  lastName: yup.string(),
  password: yup.string().min(8).max(32).required(),
  repeatPassword: yup.string().oneOf([yup.ref("password"), null], "Passwords must match"),
});

interface ISignUpForm {
  email: string;
  firstName: string;
  lastName: string;
  hearAboutUs: string;
  password: string;
  rememberMe: boolean;
  repeatPassword: string;
}

interface IFormProps {
  handleChangeStep: (description: changeSteppDirection) => void;
}

export const Form: React.FC<IFormProps> = ({ handleChangeStep }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    control,
    reset,
  } = useForm<ISignUpForm>({
    resolver: yupResolver(schema),
  });

  const watchRememberMe = watch("rememberMe");

  const submitForm = (data: ISignUpForm) => {
    reset();
    handleChangeStep("next");
  };
  return (
    <div>
      <form onSubmit={handleSubmit(submitForm)}>
        <div className="flex justify-between mb-6">
          <div className="mr-9">
            <Input
              {...register("firstName")}
              type="text"
              placeholder="John"
              label="First Name"
              error={errors.firstName?.message}
              name="firstName"
            />
          </div>
          <Input
            {...register("lastName")}
            type="text"
            placeholder="Doe"
            label="Last Name"
            name="lastName"
          />
        </div>
        <div className="mb-6">
          <Input
            {...register("email")}
            type="text"
            placeholder="johndoe@example.com"
            label="Email Address"
            error={errors.email?.message}
            name="email"
          />
        </div>

        <div className="mb-6">
          <Input
            {...register("password")}
            type="password"
            placeholder="at least 8 characters"
            label="Create Password"
            error={errors.password?.message}
            name="password"
          />
        </div>
        <div className="mb-6">
          <Input
            {...register("repeatPassword")}
            type="password"
            placeholder="repeat password"
            label="Confirm Password"
            error={errors.repeatPassword?.message}
            name="repeatPassword"
          />
        </div>

        <div className="mb-10">
          <Input
            {...register("rememberMe")}
            type="checkbox"
            name={"rememberMe"}
            label="Remember Me"
            checked={watchRememberMe}
          />
        </div>

        <p className="text-sm text-black-pearl ">How did you hear about us? (optional)</p>
        <Controller
          control={control}
          name="hearAboutUs"
          rules={{
            required: "Country is required!",
          }}
          render={({ field: { onChange, value }, fieldState: { error } }) => (
            <div>
              <ReactSelect
                className="mb-6"
                placeholder="Friends & Family"
                options={options}
                value={getValue(value)}
                onChange={(newValue) => onChange((newValue as IOption).value)}
              />
              {error && <div style={{ color: "red" }}>{error.message}</div>}
            </div>
          )}
        />

        <Button className="h-11">Sign up</Button>
      </form>
    </div>
  );
};