import { Button } from "../Button";
import { ReactComponent as Google } from "../../assets/img/svg/signUp/google.svg";
import { ReactComponent as Apple } from "../../assets/img/svg/signUp/apple.svg";
import { Form } from "./Form";
import { changeSteppDirection } from "../../types/changeSteppDirection";

interface IRegisrtrationProps{
  handleChangeStep: (description: changeSteppDirection)=> void
}

export const Regisrtration: React.FC<IRegisrtrationProps> = ({handleChangeStep}) => {
  const onClickGoogleBtn = ()=>{}
  const onClickAppleBtn = ()=>{}

  return (
    <div><div className="mb-8">
    <p className="text-2xl text-black-pearl mb-2">Sign up</p>
    <p className="text-sm text-slate-500 mb-10">
      We’ll get you up and running so you can verify your personal information and customize
      your account.
    </p>
    <div className="flex mb-6">
      <Button onClick={onClickGoogleBtn} className="flex justify-center items-center h-11 bg-gray-200 mr-4">
        <Google className="mr-2.5" />
        <span className="text-sm text-black text-opacity-75">Sign up with Google</span>
      </Button>
      <Button onClick={onClickAppleBtn} className="flex justify-center items-center h-11 bg-black-pearl">
        <Apple className="mr-2.5" />
        <span className="text-sm text-white text-opacity-75">Sign up with Apple</span>
      </Button>
    </div>
    <div className="flex justify-center">
      <span className="relative text-sm text-slate-600 before:absolute before:top-1 before:-left-24 before:block before:w-20 before:h-0.5 before:bg-slate-600 after:absolute after:top-1 after:-right-24 after:block after:w-20 after:h-0.5 after:bg-slate-600">
        or sign up with
      </span>
    </div>
  </div>
  <Form handleChangeStep={handleChangeStep}/>
  <p className="text-center mt-6">
    Already have an account? <span className="text-emerald-700">Login here</span>
  </p></div>
  )
}
