interface ICheckBoxProps {
  className?: string;
  setSelectedBtn: React.Dispatch<React.SetStateAction<string | undefined>>;
  name: string | undefined;
  checked: boolean
}

export const Radio: React.FC<ICheckBoxProps> = ({ className, name, setSelectedBtn, checked }) => {
  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    setSelectedBtn(event.target.value);
  };

  let baseClasses = [
    "h-4 w-4 shrink-0 appearance-none border-2 rounded-full",
    checked ? " border-emerald-700" : "border-slate-500",
  ];

  if (className) {
    baseClasses = [...baseClasses, ...className.split(" ")];
  }

  return (
    <label className="flex items-center cursor-pointer">
      <div className="relative flex items-center">
        <input
          className={baseClasses.join(" ")}
          type="radio"
          checked={checked}
          onChange={handleChange}
          value={name}
        />
        <div
          className={`w-2 h-2 rounded-full  absolute left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 ${
            checked ? "bg-emerald-700" : "bg-primary-bg"
          }`}
        ></div>
      </div>
      <div className="ml-3.5 text-ellipsis overflow-hidden whitespace-nowrap">{name}</div>
    </label>
  );
};