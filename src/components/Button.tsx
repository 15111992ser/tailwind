import React, { ReactNode } from "react";

interface IButtonProps {
  type?: "primary" | "show" | "arrow";
  children: ReactNode;
  onClick?: () => void;
  disabled?: boolean;
  className?: string;
}

const color = {
  primary: "text-white",
  show: "text-emerald-700",
  arrow: "text-white",
};

const backgroundColors = {
  primary: "bg-emerald-700",
  show: "bg-transparent",
  arrow: "bg-transparent"
};

const border = {
  primary: "border-none",
  show: "border-none",
  arrow: "border-2 border-gray-300"
};

export const Button: React.FC<IButtonProps> = ({
  type = "primary",
  children,
  onClick,
  className = "",
  disabled = false,
}) => {
  const disabledStyle = disabled
    ? "opacity-50 cursor-not-allowed"
    : "transition ease-in-out duration-300 hover:cursor-pointer";

  let baseClasses = [
    "rounded",
    "w-full",
    backgroundColors[type],
    color[type],
    border[type],
    disabledStyle,
  ];

  if (className) {
    baseClasses = [...baseClasses, ...className.split(" ")];
  }

  return (
    <button onClick={onClick} className={baseClasses.join(" ")} disabled={disabled}>
      {children}
    </button>
  );
};
