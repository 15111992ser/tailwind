import React, { PropsWithChildren, useRef } from "react";
import { ITableConfig } from "../../types/tableConfig";
import { useScrollbar } from "./useScrollBar";

interface ITableProps<T extends Record<string, unknown>> {
  data: T[];
  tableConfig: ITableConfig<T>[];
  className?: string;
}

export const Table = <T extends Record<string, unknown>> ({ data, tableConfig, className }: PropsWithChildren<ITableProps<T>>) => {
  const todoWrapper = useRef(null);
  const hasScroll = data.length > 10
  useScrollbar(todoWrapper, hasScroll);

  let baseClasses = ["w-full"];
  if (className) {
    baseClasses = [...baseClasses, ...className.split(" ")];
  }
  return (
    <div  style={{ height: hasScroll ? '500px' : 'auto', minHeight: '500px' }} ref={todoWrapper}>
      <table className={baseClasses.join(" ")} >
        <thead className=" border-b border-primary-border text-slate-500">
          <tr>
            {tableConfig.map((config) => {
              return (
                <td key={config.key} width={`${100 / tableConfig.length}%`} className="px-2 py-5">
                  {config.label.toLocaleUpperCase()}
                </td>
              );
            })}
          </tr>
        </thead>
        <tbody >
          {data.map((item) => {
            return (
              <tr  className=" border-b border-primary-border">
                {tableConfig.map((config) => (
                  <td className={`px-2 py-4 ${config.style}`}>{config.render(item)}</td>
                ))}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};