import React, { useState } from "react";
import { ReactComponent as Search } from "../../assets/img/svg/table/search.svg";
import { ReactComponent as Filter } from "../../assets/img/svg/table/filter.svg";

interface ITableSearchProps {
  onChangeSearch: (value: string) => void
}

export const TableSearch: React.FC<ITableSearchProps> = ({onChangeSearch}) => {
const [value, setValue] = useState("")

const onChangeHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) =>{
  const currentValue = e.target.value
  setValue(currentValue)
  onChangeSearch(currentValue)
}

  return (
    <div className="p-3 bg-gray-100 flex justify-between items-center">
      <div className="relative">
        <input
        value={value}
        onChange = {onChangeHandler}
          className="h-10 w-96 rounded p-2 pl-8 placeholder:text-sm border border-primary-border outline-none text-black-pearl bg-gray-50"
          placeholder="Search by invoice number, name, amount..."
        />
        <Search className="absolute top-1/2 left-2.5 -translate-y-1/2 " />
      </div>
      <div className="p-3 flex items-center bg-gray-50">
        <Filter className="mr-3" />
        <span className="text-slate-500">Filter</span>
      </div>
    </div>
  );
};