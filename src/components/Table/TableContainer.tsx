import React, { useState } from "react";
import { Menu } from "../Menu/Menu";
import { TableSearch } from "./TableSearch";
import { mockTableData } from "./tableData";
import { Table } from "./Table";
import { getBillingTableConfig } from "./tableConfig";
import { Header } from "./Header";

export const TableContainer = () => {
  const [data, setData] = useState(mockTableData);
  const [activeTab, setActiveTab] = useState(0);

  const onClickCheckBox = (checked: boolean, id: number) => {};
  const onClickAddBtn = () => {};
  const onClickDownloadBtn = () => {};
  const onClickPayBtn = () => {};
  const onChangeSearch = (value: string) => {};
  const onTabClick = (id: number) => {
    setActiveTab(id);
  };

  const config = getBillingTableConfig({ onClickPayBtn, onClickCheckBox});
 
  return (
    <div className="py-14 px-10">
      <Header onClickAddBtn={onClickAddBtn} onClickDownloadBtn={onClickDownloadBtn} />
      <div className="relative mb-8">
        <div className="w-80">
          <Menu
            tabList={[{ name: "Overview" }, { name: "Segments" }, { name: "Dashboard" }]}
            onTabPress={onTabClick}
            styleBottomLine="bg-emerald-700 h-0.5 rounded-none mt-2 "
          />
          <div className="border-b-2 border-primary-border absolute w-full left-0 bottom-3 -z-10"></div>
        </div>
      </div>
      <TableSearch onChangeSearch={onChangeSearch} />
      <div className="mt-8">
        {[<Table tableConfig={config}  data={data}/>,<div>Segments</div> , <div>Dashboard</div>].map(
          (element, i) => {
            if (i === activeTab) return element;
          }
        )}
      </div>
    </div>
  );
};