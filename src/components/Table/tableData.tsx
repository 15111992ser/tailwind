import { format } from "date-fns";

export const mockTableData = [
  {
    id: 1,
    invoiceNumber: 5146846548465,
    billingDate: format(new Date(2021, 12, 19), "MM/dd/yy"),
    vendor: "Jane Cooper",
    amount: "$500.00",
    status: "Paid",
    btn: "Pay",
  },
  {
      id: 2,
      vendor: "Wade Warren",
      billingDate: format(new Date(2021, 2, 19), 'MM/dd/yy'),
      invoiceNumber: 5467319467348,
      status: "Paid",
      amount: "$500.00",
      btn: "Pay"
  },
  {
      id: 3,
      invoiceNumber: 1345705945446,
      vendor: "Esther Howard",
      billingDate: format(new Date(2016, 5, 16), 'MM/dd/yy'),
      status: "Paid",
      amount: "$500.00",
      btn: "Pay"
  },
  {
      id: 4,
      invoiceNumber: 5440754979777,
      vendor: "Guy Hawkins",
      billingDate: format(new Date(2016, 9, 18), 'MM/dd/yy'),
      status: "Unpaid",
      amount: "$500.00",
      btn: "Pay"
  },
  {
      id: 5,
      invoiceNumber: 1243467984543,
      vendor: "Cameron Williamson",
      billingDate: format(new Date(2012, 4, 12), 'MM/dd/yy'),
      status: "Unpaid",
      amount: "$500.00",
      btn: "Pay"
  },
  {
      id: 6,
      invoiceNumber: 8454134649707,
      vendor: "Brooklyn Simmons",
      billingDate: format(new Date(2018, 1, 20), 'MM/dd/yy'),
      status: "Paid",
      amount: "$500.00",
      btn: "Pay"
  },
  {
      id: 7,
      invoiceNumber: 2130164040451,
      vendor: "Leslie Alexander",
      billingDate: format(new Date(2017, 4, 13), 'MM/dd/yy'),
      status: "Paid",
      amount: "$500.00",
      btn: "Pay"
  },
  {
      id: 8,
      invoiceNumber: 8454134649707,
      vendor: "Jenny Wilson",
      billingDate: format(new Date(2022, 3, 14), 'MM/dd/yy'),
      status: "Unpaid",
      amount: "$500.00",
      btn: "Pay"
  }
];