import { ITableConfig } from "../../types/tableConfig";
import { Button } from "../Button";
import { CheckBox } from "../CheckBox";

type ITableItem = {
  id: number;
  invoiceNumber: number;
  billingDate: string;
  vendor: string;
  amount: string;
  status: string;
  btn: string;
}

interface IGetTableConfig {
  onClickPayBtn: () => void;
  onClickCheckBox: (checked: boolean, id: number) => void;
}

export const getBillingTableConfig = (params: IGetTableConfig): ITableConfig<ITableItem>[] => {
  return [
    {
      label: "Invoice Number",
      key: "invoiceNumber",
      render: (item) => {
        return (
          <CheckBox
            onClickCheckBox={(checked, value) => {
              params.onClickCheckBox(checked, item.id);
            }}
            config={{ value: false, label: String(item.invoiceNumber) }}
          />
        );
      },
    },
    {
      label: "Vendor",
      key: "vendor",
      render: (item) => {
        return <>{item.vendor}</>;
      },
    },
    {
      label: "Billin date",
      key: "billingDate",
      render: (item) => {
        return <>{item.billingDate}</>;
      },
    },
    {
      label: "Status",
      key: "status",
      style: "flex",
      render: (item) => {
        return (
          <div
            className={`px-3 py-1 rounded ${
              item.status === "Paid" ? "bg-green-200 text-green-500" : "bg-red-50 text-red-500"
            }`}
          >
            {item.status}
          </div>
        );
      },
    },
    {
      label: "Amount",
      key: "amount",
      render: (item) => {
        return <>{item.amount}</>;
      },
    },
    {
      label: "",
      key: "btn",
      style: "flex justify-center items-center",
      render: (item) => {
        return (
          <Button
            onClick={params.onClickPayBtn}
            disabled={item.status === "Paid"}
            className={`w-20 h-8 ${item.status === "Paid" && "bg-gray-300 text-slate-500"}`}
          >
            {item.btn}
          </Button>
        );
      },
    },
  ];
};