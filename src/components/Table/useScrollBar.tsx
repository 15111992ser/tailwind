import { useEffect } from "react";
import { OverlayScrollbars } from "overlayscrollbars";

export const useScrollbar = (root: React.MutableRefObject<null>, hasScroll: boolean) => {
  useEffect(() => {
    let scrollbars: any;

    if (root.current && hasScroll) {
      scrollbars = OverlayScrollbars(root.current, {
        scrollbars: {
          visibility: "auto",
          autoHide: "never",
        },
      });
    }

    return () => {
      if (scrollbars) {
        scrollbars.destroy();
      }
    };
  }, [root, hasScroll]);
};