import React from "react";
import { Button } from "../Button";
import { ReactComponent as Plus } from "../../assets/img/svg/table/plus.svg";
import { ReactComponent as Download } from "../../assets/img/svg/table/download.svg";

interface IHeaderProps {
  onClickAddBtn: ()=> void;
  onClickDownloadBtn: ()=> void
}

export const Header: React.FC<IHeaderProps> = ({onClickAddBtn, onClickDownloadBtn}) => {
  return (
    <div className="flex justify-between items-center mb-10">
      <div>
        <h3 className="text-3xl font-bold mb-2">Billing</h3>
        <p className="text-lg text-slate-500">Manage your billing and payment details</p>
      </div>
      <div className="flex ">
        <Button onClick={onClickAddBtn} className="flex items-center justify-center h-11 w-20 mr-3">
            <Plus className="mr-2.5"/>
            <span>Add </span>
        </Button>
        <Button onClick={onClickDownloadBtn} className="flex items-center justify-center h-11 w-60" type="arrow">
            <Download className="mr-2.5"/>
            <span className="font-semibold text-black-pearl">Download PDF Report </span>
        </Button>
      </div>
    </div>
  );
};