import React, { useState } from "react";
import { ISelectedFilter } from "../types/filters";
import { ReactComponent as Check } from "../assets/img/svg/filter/check.svg";

interface ICheckBoxProps {
  config: ISelectedFilter;
  className?: string;
  onClickCheckBox?: (isChecked: boolean, value: string) => void;
}

export const CheckBox: React.FC<ICheckBoxProps> = ({
  config,
  className,
  onClickCheckBox,
}) => {
  const [checked, setChecked] = useState(config.value);

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (event) => {
    const { checked, value } = event.target;
    if (onClickCheckBox) {
      onClickCheckBox(checked, value);
    }
    setChecked(checked);
  };

  let baseClasses = [
    "h-4 w-4 shrink-0 appearance-none border-2 border-primary-border rounded-sm",
    checked && " bg-emerald-700 border-emerald-700",
  ];

  if (className) {
    baseClasses = [...baseClasses, ...className.split(" ")];
  }

  return (
    <label className="flex items-center cursor-pointer ">
      <div className="relative flex items-center">
        <input
          className={baseClasses.join(" ")}
          type="checkbox"
          name={config.label}
          value={config.label}
          checked={checked}
          onChange={handleChange}
        />
        <Check className="w-3 h-2 absolute left-1/2 top-1/2 -translate-y-1/2 -translate-x-1/2" />
      </div>
      <div className="ml-3.5 text-ellipsis overflow-hidden whitespace-nowrap">{config.label}</div>
    </label>
  );
};
