import React from "react";

import { ICity, ITest } from "../../types/speeds";
import { Button } from "../Button";
import { ProgressBar } from "./ProgressBar";

interface IDataSpeedsProps {
  city: ICity;
  testInfo: ITest;
  startTest: () => void;
  speed: number;
  isLoading: boolean;
  size: number;
  speedSize: number
}
export const SpeedTest: React.FC<IDataSpeedsProps> = ({
  city,
  testInfo,
  startTest,
  speed,
  isLoading,
  size,
  speedSize = 1
}) => {
  return (
    <div className="inline-block rounded bg-white">
      <div className=" p-4 w-full h-full ">
        <div className="flex items-center justify-center mb-1.5">
          <div className="w-2 h-2 mr-2 rounded-full bg-emerald-500"></div>
          <p className="font-semibold">Connected to {city.provider}</p>
        </div>
        <div className="flex justify-center items-center">
          <div className="mr-2">
            {<city.icon/>}
          </div>
          <p className="text-slate-500 text-sm">{city.name}</p>
        </div>
        <div className="flex p-3 mb-7">
          <div className="w-4/6 flex flex-col justify-end">
            <div className="flex justify-center items-center h-full ">
              <ProgressBar progress={speed/speedSize} size={size} strokeWidth={5} />
            </div>
            <div>
              <p className="text-lg font-bold text-center">{`${speed} mbps`}</p>
            </div>
          </div>
          <div className="w-2/6 flex flex-col justify-end ml-8">
            <div className="mb-2.5">
              <p className="text-slate-500 text-sm">Upload</p>
              <p className="text-lg font-bold">{`${testInfo.upload.toFixed(0)} GB`}</p>
            </div>
            <div>
              <p className="text-slate-500 text-sm">Download</p>
              <p className="text-lg font-bold">{`${testInfo.download.toFixed(0)} GB`}</p>
            </div>
          </div>
        </div>
        <Button
          disabled={isLoading}
          className="h-10"
          onClick={startTest}
        >
          Speed Test
        </Button>
      </div>
    </div>
  );
};
