import React, { useEffect, useLayoutEffect, useRef, useState } from "react";
import { calcCoordinates } from "../../utils/calculation";
import { ReactComponent as Arrow } from "../../assets/img/svg/spedds/arrow.svg";

interface IProgressBarProps {
  size: number;
  progress: number;
  strokeWidth: number;
}

export const ProgressBar: React.FC<IProgressBarProps> = ({
  size = 100,
  progress = 0,
  strokeWidth = 5,
}) => {
  const [progressOffset, setProgressOffset] = useState(0);
  const [progressArrow, setProgressArrow] = useState(0);
  const totalSegments = 10;
  const hiddenSegments = 2;
  const baseSize = 110;
  const visibleSegments = totalSegments - hiddenSegments;
  const differenceBetweenBigAndSmallCircleCoeff = 50 / 60;
  const scaleArrow = size / baseSize;

  const center = size / 2;
  const radius = size / 2 - strokeWidth;
  const circumference = 2 * Math.PI * radius;
  const defoultStrokeDasharray = (circumference / totalSegments) * hiddenSegments + 5;

  useLayoutEffect(() => {
    setProgressOffset(circumference);
  }, []);

  useEffect(() => {
    const progressOffset = ((100 - progress) / 100) * (circumference - defoultStrokeDasharray);
    const progressArrow = (progress / 100) * ((360 / totalSegments) * visibleSegments);
    setProgressArrow(progressArrow);
    setProgressOffset(progressOffset + defoultStrokeDasharray);
  }, [progress, circumference, progressOffset, progressArrow]);

  return (
    <div className="relative ">
      <svg
        width={size}
        height={size}
        fill="transparent"
        xmlns="http://www.w3.org/2000/svg"
        className=""
      >
        <circle
          className="origin-center rotate-[129deg] stroke-emerald-500  duration-700"
          strokeLinecap="round"
          cx={center}
          cy={center}
          r={radius}
          strokeWidth={strokeWidth}
          strokeDasharray={`${circumference} ${circumference}`}
          strokeDashoffset={progressOffset}
        />
        <circle className="z-40" cx="50%" cy="50%" r="2.7027" fill="#191D23" />

        {calcCoordinates(
          radius * differenceBetweenBigAndSmallCircleCoeff,
          totalSegments,
          size / 2
        ).map(
          (el, i) =>
            i > 0 && <circle key={el[0]} cx={el[0]} cy={el[1]} r="1.35135" fill="#10B981" />
        )}
      </svg>
      <Arrow
        style={{ transform: `rotate(151deg) rotate(${progressArrow}deg) scale(${scaleArrow})` }}
        className="absolute  bottom-1/2 left-1/2 origin-bottom-left duration-700"
      />
    </div>
  );
};
