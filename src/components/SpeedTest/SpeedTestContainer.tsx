import React, { useEffect, useRef, useState } from "react";
import { SpeedTest } from "./SpeedTest";
import { ReactComponent as Flags } from "../../assets/img/svg/spedds/flags.svg";
import { randomSpeed } from "../../utils/randomSpeed";
import { average } from "../../utils/average";
import { ICity, ITest } from "../../types/speeds";

const data = {
  city: { name: "USA", icon: Flags, provider: "Hypernet23" },
  test: {
    upload: 0,
    download: 0,
  },
};

interface ISpeedTestContainerProps {}

export const SpeedTestContainer: React.FC<ISpeedTestContainerProps> = () => {
  const [city, setCity] = useState<ICity>(data.city);
  const [testInfo, setTestInfo] = useState<ITest>(data.test);
  const [speed, setSpeed] = useState<number>(0);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const speeds: React.MutableRefObject<number[]> = useRef([0]);

  const testProcess = () => {
    const num = randomSpeed(3);
    setSpeed(num);
    speeds.current.push(num);
  };

  const sleep = (delay: number) => {
    return new Promise((resolve)=>{
      setTimeout(() => { resolve(true)}, delay);
    })
  };

  const startTest = async () => {
    setTestInfo({ upload: 0, download: 0 });
    let i = 0;

    const test = (nameTest: string) => {
      return new Promise((resolve) => {
        setIsLoading(true);
        const intervalId = setInterval(() => testProcess(), 250);
        setTimeout(() => {
          i++;
          clearInterval(intervalId);
          setTestInfo((prev) => ({ ...prev, [nameTest]: average(speeds.current) }));
          setSpeed(0);
          if (i > 1) setIsLoading(false);
          resolve(true);
        }, 5000);
      });
    };

    await test("download");
    await sleep(1000);
    await test("upload");
  };

  return (
    <div className="flex justify-center">
      <SpeedTest
        testInfo={testInfo}
        city={city}
        startTest={startTest}
        speed={speed}
        isLoading={isLoading}
        size={200}
        speedSize={3}
      />
    </div>
  );
};
