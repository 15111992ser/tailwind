
import fileDefault from '../../assets/img/png/file-blank-solid-240.png';
import fileCSS from '../../assets/img/png/file-css-solid-240.png';
import filePdf from '../../assets/img/png/file-pdf-solid-240.png';
import filePng from '../../assets/img/png/file-png-solid-240.png';

export type ImageConfigKeys = keyof typeof ImageConfig;

export const ImageConfig = {
    default: fileDefault,
    pdf: filePdf,
    png: filePng,
    css: fileCSS,
}