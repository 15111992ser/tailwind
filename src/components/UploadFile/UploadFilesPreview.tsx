import React from "react";
import { ReactComponent as Remove } from "../../assets/img/svg/uploadFile/remove.svg";
import { Button } from "../Button";
import { ImageConfig, ImageConfigKeys } from "./imgConfig";

interface IUploadFilesPreviewProps {
  fileRemove: (file: File) => void;
  fileList: File[];
}

export const UploadFilesPreview: React.FC<IUploadFilesPreviewProps> = ({ fileList, fileRemove }) => {
  return (
    <>
      {fileList.length > 0 ? (
        <div>
          <p className="text-sm text-center my-2 ">Ready to upload</p>
          {fileList.map((item, index) => {
           return <div className="flex justify-between items-center border border-dashed border-primary-border rounded mb-2 p-2">
              <div className="flex">
                <div className="w-10 h-10 mr-4">
  
                  <img src={ImageConfig[item.type.split("/")[1] as ImageConfigKeys]  || ImageConfig["default"]} alt={item.name} />
                </div>
                <div className="text-xs">
                  <p className="mb-1 font-semibold">{item.name}</p>
                  <p >{(item.size / 1024).toFixed(2)} KB</p>
                </div>
              </div>
              <Button onClick={()=> fileRemove(item)} className="w-5 h-5 bg-transparent hover:bg-red-200 rounded-full">
                <Remove className="m-auto" />
              </Button>
            </div>
          })}
        </div>
      ) : null}
    </>
  );
};
