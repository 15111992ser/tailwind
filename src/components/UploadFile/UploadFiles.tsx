import React, { useState } from "react";
import { ReactComponent as UploadIcon } from "../../assets/img/svg/uploadFile/upload.svg";
import { ReactComponent as PlusIcon } from "../../assets/img/svg/uploadFile/plus.svg";
import { Button } from "../Button";
import { UploadFilesPreview } from "./UploadFilesPreview";

interface IUploadFilesProps {
  onFileChange: (files: (File)[]) => void;
  fileList: File[]
}

export const UploadFiles: React.FC<IUploadFilesProps> = ({ onFileChange, fileList }) => {
  const [drag, setDrag] = useState(false);

  const onChangeInputHandler: React.ChangeEventHandler<HTMLInputElement> | undefined = (e) => {
    if (e.target.files) {
      const newFile = e.target.files;
      const updatedList = [...fileList, ...newFile];
      onFileChange(updatedList);
    }
  };

  const dragStartHandler: React.DragEventHandler<HTMLDivElement> | undefined = (e) => {
    e.stopPropagation();
    e.preventDefault();
    setDrag(true);
  };

  const dragLeaveHandler: React.DragEventHandler<HTMLDivElement> | undefined = (e) => {
    e.preventDefault();
    if (e.currentTarget.contains(e.relatedTarget as Node)) return;
    setDrag(false);
  };

  const dragDropHandler: React.DragEventHandler<HTMLDivElement> | undefined = (e) => {
    e.preventDefault();
    const files = [...e.dataTransfer.files];
    const updatedList = [...fileList, ...files];
    onFileChange(updatedList);
    setDrag(false);
  };

  const fileRemove = (file: File) => {
    const updatedList = [...fileList];
    updatedList.splice(fileList.indexOf(file), 1);
    onFileChange(updatedList);
}

  return (
    <div>
      <div
        onDragOver={(e) => dragStartHandler(e)}
        onDragLeave={(e) => dragLeaveHandler(e)}
        onDrop={(e) => dragDropHandler(e)}
        className="border border-dashed border-primary-border rounded p-4 h-40 flex flex-col justify-between "
      >
        <div className=" flex justify-center">
          <UploadIcon />
        </div>
        <p className="text-sm text-black-pearl text-center ">
          {drag
            ? "Drop the file"
            : "Browse and chose the files you want to upload from your computer"}
        </p>
        <div className="flex justify-center">
          <Button className="w-8 h-8 relative">
            <PlusIcon className="m-auto" />
            <label className="cursor-pointer absolute top-0 left-0 w-full h-full">
              <input multiple className="hidden" type="file" onChange={onChangeInputHandler} />
            </label>
          </Button>
        </div>
      </div>
      <UploadFilesPreview fileList={fileList} fileRemove={fileRemove}/>
    </div>
  );
};