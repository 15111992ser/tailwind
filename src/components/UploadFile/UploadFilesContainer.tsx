import React, { useState } from "react";
import { Button } from "../Button";
import { UploadFiles } from "./UploadFiles";
import { ReactComponent as Arrow } from "../../assets/img/svg/uploadFile/arrow.svg";

export const UploadFilesContainer = () => {
  const [fileList, setFileList] = useState<File[]>([]);

  const onFileChange = (files: File[]) => {
    setFileList(files)
  };

  return (
    <div className="p-4 w-96 border border-primary-border mx-auto">
      <div className="flex items-center mb-4">
        <Button type="arrow" className="mr-3 w-5 h-5">
          <Arrow className="m-auto" />
        </Button>
        <p className="font-semibold">Upload Resume</p>
      </div>
      <UploadFiles fileList={fileList} onFileChange={onFileChange}/>
    </div>
  );
};