import React, { ReactNode } from "react";

interface ICounterProps {
  className?: string;
  onClick?: () => void;
  children: ReactNode;
  type: "black" | "green";
}

const backgroundColors = {
  black: "bg-black-pearl",
  green: "bg-emerald-700",
};

export const Counter: React.FC<ICounterProps> = ({ className, onClick, children, type }) => {
  let baseClasses = ["w-10", "h-6", "rounded", "text-white", "font-semibold", "text-center", backgroundColors[type]];

  if (className) {
    baseClasses = [...baseClasses, ...className.split(" ")];
  }

  return (
    <div onClick={onClick} className={baseClasses.join(" ")}>
      {children}
    </div>
  );
};
