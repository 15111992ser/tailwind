import { ReactComponent as Overview } from "../../assets/img/svg/sideBar/overview.svg";
import { ReactComponent as Research } from "../../assets/img/svg/sideBar/research.svg";
import { ReactComponent as Reports } from "../../assets/img/svg/sideBar/reports.svg";
import { ReactComponent as Teams } from "../../assets/img/svg/sideBar/teams.svg";
import { ReactComponent as Notifications } from "../../assets/img/svg/sideBar/notifications.svg";
import { ReactComponent as Settings } from "../../assets/img/svg/sideBar/settings.svg";
import { ISideBarData } from "../../types/sideBar";

export const sideBarListData: Array<ISideBarData>  = [
  { name: "Overview", svg: Overview},
  { name: "Research", svg: Research },
  { name: "Reports", svg: Reports, counter: {value: 10, color: "black"}},
  { name: "Teams", svg: Teams, sublist: ["Analytics", "Design", "Engineering", "Marketing"] },
  { name: "Notifications", svg: Notifications, counter: {value: 10, color: "green"}},
  { name: "Settings", svg: Settings },
];