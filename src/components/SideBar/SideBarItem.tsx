import React, { useState } from "react";
import { ReactComponent as Arrow } from "../../assets/img/svg/sideBar/arrow.svg";
import { Counter } from "./Counter";

interface ISideBarItemProps {
  name: string;
  isActiveItem?: boolean;
  itemSublist?: Array<string> | undefined;
  SvgComponent: React.FC<React.SVGProps<SVGSVGElement> & { title?: string | undefined }>;
  counter: { value: number; color: "black" | "green" } | undefined;
}

export const SideBarItem: React.FC<ISideBarItemProps> = ({
  name,
  isActiveItem,
  itemSublist,
  SvgComponent,
  counter,
}) => {
  const [activeSubItem, setIsActiveSubItem] = useState<number | null>(null);
  const [isOpenSubMenu, setIsOpenSubMenu] = useState<boolean>(false);

  return (
    <div className="mb-4">
      <div
        onClick={() => {setIsOpenSubMenu(!isOpenSubMenu)}}
        className={`flex justify-between items-center h-12  cursor-pointer px-3 py-2 rounded sm:justify-center ${
          isActiveItem && "bg-active-item" }`}>
        <div className="flex justify-between items-center">
          <SvgComponent className="w-6 " />
          <span className="ml-2 sm:hidden">{name}</span>
        </div>
        {counter && (
          <Counter className="sm:hidden" type={counter?.color}>
            {counter.value}
          </Counter>
        )}
        {itemSublist && <Arrow className={`${isOpenSubMenu ? "rotate-135" : "rotate-180"} sm:hidden`} />}
      </div>
      
      {itemSublist && isOpenSubMenu && (
        <div className="border-l border-l-primary-border ml-6 mr-2 mt-2 cursor-pointer sm:hidden">
          {itemSublist.map((item, index) => (
            <div onClick={() => { setIsActiveSubItem(index); }} key={item}>
              <div
                className={`flex justify-between items-center p-2 ml-4 rounded 
                ${activeSubItem === index && "bg-active-item"}`}
              >
                <span>{item}</span>
                {activeSubItem === index && <Arrow className="rotate-90" />}
              </div>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};
