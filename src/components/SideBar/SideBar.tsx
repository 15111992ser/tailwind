import React, { useState } from "react";
import { ISideBarData } from "../../types/sideBar";
import { Avatar } from "../Avatar";
import { Button } from "../Button";
import { SideBarItem } from "./SideBarItem";
import { ReactComponent as Logo } from "../../assets/img/svg/sideBar/logo.svg";
import { ReactComponent as LogOut } from "../../assets/img/svg/sideBar/logout.svg";
import { ReactComponent as Plus } from "../../assets/img/svg/sideBar/plus.svg";

interface ISideBarProps {
  sideBarList: Array<ISideBarData>;
  authUser: { email: string; name: string; avatar: string };
}

export const SideBar: React.FC<ISideBarProps> = ({ sideBarList, authUser }) => {
  const [selectedItem, setSelectedItem] = useState<number | null>(null);

  return (
    <div className="flex">
      <div className={`p-6 shadow-xl flex flex-col bg-white font-semibold`}>
        <div className="flex items-center mb-6 sm:justify-center">
          <Logo className="w-9 h-8 mr-2 cursor-pointer" />
          <h2 className="font-bold text-transparent text-2xl bg-clip-text bg-gradient-to-r from-gradient-logo-from to-emerald-600 sm:hidden">
            AcmeSystems
          </h2>
        </div>
        <Button className="h-11 mb-6 ">
          <div className="flex items-center justify-center">
            <Plus className="h-4 w-4" />
            <span className="ml-2 sm:hidden">New Report</span>
            <span className="ml-1 hidden text-sm sm:block">Report</span>
          </div>
        </Button>
        <div className="border-b-2 mb-6 ">
          <div className="text-slate-500 mb-4 sm:text-center">Menu</div>
          {sideBarList.map((item, index) => (
            <div
              onClick={() => {
                setSelectedItem(index);
              }}
              key={item.name}
            >
              <SideBarItem
                name={item.name}
                isActiveItem={index === selectedItem}
                itemSublist={item.sublist}
                SvgComponent={item.svg}
                counter={item.counter}
              />
            </div>
          ))}
        </div>
        <div className="flex-col font-semibold sm:text-center">
          <p className="text-slate-500 mb-4">Profile</p>
          <div className="flex mb-6 items-center justify-center">
            <Avatar imgUrl={authUser.avatar} />
            <div className=" ml-2 sm:hidden">
              <p>{authUser.name}</p>
              <p className="text-slate-500">{authUser.email}</p>
            </div>
          </div>
          <p className="flex justify-center items-center cursor-pointer sm:hidden">
            <LogOut className="w-5 h-5 mr-2" />
            <span className="block">Log out</span>
          </p>
        </div>
      </div>
    </div>
  );
};
