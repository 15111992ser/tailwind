export const calcCoordinates = (radius: number, totalPoint: number, widthSide: number) => {
  const step = 360 / totalPoint;
  let x;
  let y;
  let angle = 0;

  const result = [];
  for (let i = 0; i < totalPoint; i++) {
    x = widthSide - Math.sin(angle / 57.2958) * radius;
    y = Math.cos(angle / 57.2958) * radius + widthSide;
    result.push([x, y]);
    angle += step;
  }
  return result;
};
