export const formatSongDuration = (songLenght: number | undefined) => {
  if (songLenght) {
    const min = String(Math.trunc(Math.trunc(songLenght) / 60)).padStart(2, "0");
    const sec = String(Math.trunc(songLenght) - +min * 60).padStart(2, "0");

    return `${min}:${sec}`;
  }
};
