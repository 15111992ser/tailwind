export const average = (nums: number[]) => {
  return nums.reduce((a, b) => a + b) / nums.length;
};
