export const getWeekNumber = (date: Date) => {
    const millisecondsOfDay = 86400000
    const firstDayOfTheYear = new Date(date.getFullYear(), 0, 1);
    
    const pastDaysOfYear = (date.getTime() - firstDayOfTheYear.getTime()) / millisecondsOfDay;
    return Math.ceil((pastDaysOfYear + firstDayOfTheYear.getDay() + 1) / 7);
  };
  