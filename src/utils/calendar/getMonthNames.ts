import { IGetMonthesNamesResponse } from "../../types/getMonthesNamesResponse";
import { createDate } from "./createDate";

export const getMonthesNames = (locale: string = "defalut"): Array<IGetMonthesNamesResponse> => {
  const monthesNames = [];
  const d = new Date();

  for (let index = 0; index < 12; index++) {
    const { month, monthIndex, monthShort, date } = createDate({
      locale,
      date: new Date(d.getFullYear(), index, 1),
    });

    monthesNames.push({ month, monthIndex, monthShort, date });
  }
  return monthesNames;
};
