import { ISelectedPeriod } from "../../types/selectedPeriod";

export const checkIsPeriod = (day: Date, selectedPeriod?: ISelectedPeriod | null | undefined, ) => {
  const from = selectedPeriod?.from;
  const to = selectedPeriod?.to;

  let start;
  let end;
  if (from && to) {
    if (from < to) {
      start = from;
      end = to;
    } else {
      start = to;
      end = from;
    }
    const isInPeriod = start.getTime() < day.getTime() && day.getTime() < end.getTime();
    const startOrEndPeriod = day.getTime() === start.getTime() || day.getTime() === end.getTime();

    return { isInPeriod, startOrEndPeriod };
  } else {
    start = from || to;
    const startOrEndPeriod = day?.getTime() === start?.getTime();
    return { isInPeriod: false, startOrEndPeriod };
  }
};
