import { IGetWeeksNamesResponse } from "../../types/getWeeksNamesResponse";
import { createDate } from "./createDate";

export const getWeekDaysNames = (
  firstWeekDay: number = 4,
  locale: string = "default"
): Array<IGetWeeksNamesResponse> => {
  const weekDaysNames = [];
  const date = new Date();

  for (let index = 0; index < 7; index++) {
    const { day, dayShort } = createDate({
      locale,
      date: new Date(date.getFullYear(), date.getMonth(), index),
    });
    weekDaysNames.push({ day, dayShort });
  }

  return [...weekDaysNames.slice(firstWeekDay - 1), ...weekDaysNames.slice(0, firstWeekDay - 1)];
};
